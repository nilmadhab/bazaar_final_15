from django.conf.urls import patterns, include, url

from django.contrib import admin
import django_cron
import settings

admin.autodiscover()
django_cron.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ktj14.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    #Admin urls
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    #url(r'^cache.appcache$','index.views.manifest',name='manifest'),
    #Access urls
    url(r'^accounts/', include("users.urls")),
    url(r'^woodstock/', include("woodstock.urls")),
    url(r'^algotrade/', include('algotrade.urls')),
    url(r'^', include("index.urls")),
)
urlpatterns += staticfiles_urlpatterns()
