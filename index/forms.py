'''
Created on Jun 12, 2013

@author: Srinivasan
'''
from index.models import ForexOpenedOptions, ForexPendingOptions, \
    CommOpenedOptions, CommPendingOptions
from index.utils import current_price, find_price
from django import forms
from django.utils import timezone

LEVEARAGE_CHOICES=[(25,'1:25'),(50,'1:50'),(100,'1:100'),(200,'1:200')]
COMM_LEVEARAGE_CHOICES=[(50,'1:50'),(100,'1:100')]
BORS_CHOICES=[('Long','Long'),('Short','Short')]
CURRENCY_CHOICES=[("USD to INR","USD to INR"),("USD to CAD","USD to CAD"),("USD to JPY","USD to JPY"),("USD to GBP","USD to GBP"),("USD to EUR","USD to EUR"),("USD to CHF","USD to CHF"),("USD to AUD","USD to AUD"),("USD to NZD","USD to NZD")]

class CancelOrderForm(forms.Form):
    pendingid= forms.IntegerField(label='Pending Id')
    currency=forms.CharField(max_length=100,required=True)
    
class CommCancelOrderForm(forms.Form):
    pendingid= forms.IntegerField(label='Pending Id')
    tradename=forms.CharField(max_length=100,required=True)
    
    
class OpenTransaction(forms.Form):
    userid=forms.CharField(max_length=100)
    currency=forms.ChoiceField(CURRENCY_CHOICES)
    levearage=forms.ChoiceField(LEVEARAGE_CHOICES)
    bors=forms.ChoiceField(BORS_CHOICES)
    lots=forms.IntegerField()
    triggerprice=forms.FloatField()
    
    def save(self):
        opendatetime=timezone.now()
        openprice=current_price(self.cleaned_data['currency'],self.cleaned_data['bors'])
        forex_opened_option=ForexOpenedOptions()
        forex_opened_option.userid=self.cleaned_data['userid']
        forex_opened_option.currency=self.cleaned_data['currency']
        forex_opened_option.levearage=self.cleaned_data['levearage']
        forex_opened_option.bors=self.cleaned_data['bors']
        forex_opened_option.lots=self.cleaned_data['lots']
        forex_opened_option.triggerprice=self.cleaned_data['triggerprice']
        forex_opened_option.opendateandtime=opendatetime
        forex_opened_option.openprice=openprice
        forex_opened_option.save()
        
        
class OpenCommTransaction(forms.Form):
    userid=forms.CharField(max_length=100)
    commodity=forms.CharField(max_length=100)
    levearage=forms.ChoiceField(COMM_LEVEARAGE_CHOICES)
    bors=forms.ChoiceField(BORS_CHOICES)
    lots=forms.IntegerField()
    triggerprice=forms.FloatField()
    
    def save(self):
        opendatetime=timezone.now()
        openprice=find_price(self.cleaned_data['commodity'])
        comm_opened_option=CommOpenedOptions()
        comm_opened_option.userid=self.cleaned_data['userid']
        comm_opened_option.tradename=self.cleaned_data['commodity']
        comm_opened_option.levearage=self.cleaned_data['levearage']
        comm_opened_option.bors=self.cleaned_data['bors']
        comm_opened_option.quantity=self.cleaned_data['lots']
        comm_opened_option.triggerprice=self.cleaned_data['triggerprice']
        comm_opened_option.opendateandtime=opendatetime
        comm_opened_option.openprice=openprice
        comm_opened_option.save()
        
        
class PendingTransaction(forms.Form):
    userid=forms.CharField(max_length=100)
    currency=forms.ChoiceField(CURRENCY_CHOICES)
    levearage=forms.ChoiceField(LEVEARAGE_CHOICES)
    bors=forms.ChoiceField(BORS_CHOICES)
    lots=forms.IntegerField()
    triggerprice=forms.FloatField()
    pendingprice=forms.FloatField()
    
    def save(self):
        forex_pending_option=ForexPendingOptions()
        forex_pending_option.userid=self.cleaned_data['userid']
        forex_pending_option.currency=self.cleaned_data['currency']
        forex_pending_option.levearage=self.cleaned_data['levearage']
        forex_pending_option.bors=self.cleaned_data['bors']
        forex_pending_option.lots=self.cleaned_data['lots']
        forex_pending_option.triggerprice=self.cleaned_data['triggerprice']
        forex_pending_option.pendingprice=self.cleaned_data['pendingprice']
        forex_pending_option.save()
        
class CommPendingTransaction(forms.Form):
    userid=forms.CharField(max_length=100)
    commodity=forms.CharField(max_length=100)
    levearage=forms.ChoiceField(COMM_LEVEARAGE_CHOICES)
    bors=forms.ChoiceField(BORS_CHOICES)
    lots=forms.IntegerField()
    triggerprice=forms.FloatField()
    pendingprice=forms.FloatField()
    
    def save(self):
        comm_pending_option=CommPendingOptions()
        comm_pending_option.userid=self.cleaned_data['userid']
        comm_pending_option.tradename=self.cleaned_data['commodity']
        comm_pending_option.levearage=self.cleaned_data['levearage']
        comm_pending_option.bors=self.cleaned_data['bors']
        comm_pending_option.quantity=self.cleaned_data['lots']
        comm_pending_option.triggerprice=self.cleaned_data['triggerprice']
        comm_pending_option.pendingprice=self.cleaned_data['pendingprice']
        comm_pending_option.save()