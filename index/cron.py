'''
Created on Jun 9, 2013

@author: Srinivasan
'''
from django_cron import cronScheduler, Job
from cron_functions import *


''' Pattern for writing Cron Job
class Class_Name(Job):

        # run every 300 seconds (5 minutes)
        run_every = 300
                
        def job(self):
                # This will be executed every 5 minutes
                taskToDo()

cronScheduler.register(Class_Name)'''

class UpdateOpenPrice(Job):
    
        run_every = 60*60*24
                
        def job(self):
                get_open()

cronScheduler.register(UpdateOpenPrice)

class UpdateStockPrice(Job):
    
        run_every = 60
                
        def job(self):
                stockprice()

cronScheduler.register(UpdateStockPrice)

class UpdateCommodityPrice(Job):
    
        run_every = 62
                
        def job(self):
                commodityprice()

cronScheduler.register(UpdateCommodityPrice)

class UpdateIndices(Job):
    
        run_every = 3600
                
        def job(self):
                get_indices()

cronScheduler.register(UpdateIndices)

class UpdateForexRanks(Job):

        run_every = 1800
                
        def job(self):
                updateforexranks()
                
cronScheduler.register(UpdateForexRanks)

class ForexTrigClose(Job):

        run_every = 600
                
        def job(self):
                forex_trigclose()
                
cronScheduler.register(ForexTrigClose)

class CommTrigClose(Job):

        run_every = 700
                
        def job(self):
                comm_trigclose()
                
cronScheduler.register(CommTrigClose)

class CommPending(Job):

        run_every = 710
                
        def job(self):
                comm_pending()
                
cronScheduler.register(CommPending)

class ForexPending(Job):

        run_every = 610
                
        def job(self):
                forex_pending()
                
cronScheduler.register(ForexPending)



