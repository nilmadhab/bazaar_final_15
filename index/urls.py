'''
Created on Jun 12, 2013

@author: Srinivasan
'''
from django.conf.urls import patterns, url


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ktj14.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'index.views.index',name='index'),
    url(r'^forex$', 'index.views.index',name='index'),
    url(r'^commodity/terminal$', 'index.views.commodity_terminal',name='comm_terminal'),
    url(r'^forex/history$', 'index.views.history',name='history'),
    url(r'^commodity/history$', 'index.views.commodity_history',name='comm_history'),
    url(r'^forex/terminal$', 'index.views.terminal',name='terminal'),
    url(r'^forex/closetrade$', 'index.views.closetrade',name='closetrade'),
    url(r'^forex/closetradeall$', 'index.views.closetradeall',name='closetradeall'),
    url(r'^commodity/closetrade$', 'index.views.commclosetrade',name='commclosetrade'),
    url(r'^commodity/closetradeall$', 'index.views.commclosetradeall',name='commclosetradeall'),
    url(r'^commodity/terminal_list$', 'index.views.comm_terminal_list',name='comm_terminal_list'),
    url(r'^forex/trade$', 'index.views.trade',name='trade'),
    url(r'^forex/terminal_list$', 'index.views.terminal_list',name='terminal_list'),
    url(r'^time$', 'index.views.time',name='time'),
    url(r'^indices$', 'index.views.indices',name='indices'),
    url(r'^forex/stocks$', 'index.views.stocks_marquee',name='stocks_marquee'),
    url(r'^forex/basics$', 'index.views.forex_basics',name='forex_basics'),
    url(r'^forex/cancel_order$', 'index.views.cancelbuyorder',name='cancel_buy_order'),
    url(r'^commodity/cancel_order$', 'index.views.cancelbuyordercomm',name='comm_cancel_buy_order'),
    url(r'^forex/transaction', 'index.views.transaction',name='transaction'),
    url(r'^forex/commodity_transaction', 'index.views.commodity_transaction',name='commodity_transaction'),
    url(r'^forex/transact$', 'index.views.transact',name='transact'),
    url(r'^forex/transact_commodity$', 'index.views.transact_commodity',name='transact_commodity'),
    url(r'^forex/help$', 'index.views.help',name='help'),
    url(r'^forex/home$', 'index.views.home',name='home'),
    url(r'^forex/info$', 'index.views.price_info',name='info'),
    url(r'^forex/interface$', 'index.views.interface',name='interface'),
    url(r'^forex/learn$', 'index.views.learn',name='learn'),
    url(r'^forex/pendingorders$', 'index.views.pendingorders',name='pendingorders'),
    url(r'^commodity/pendingorders$', 'index.views.commpendingorders',name='commpendingorders'),
    url(r'^forex/statistics$', 'index.views.statistics',name='statistics'),
    url(r'^forex/toprankers$', 'index.views.toprankers',name='toprankers'),
    url(r'^forex/charts/(?P<type>\w+)/(?P<identifier>\w+)/$', 'index.views.charts',name='charts'),
    url(r'^forex/not_allowed$', 'index.views.not_allowed',name='not_allowed'),
    url(r'^forex/$', 'index.views.index',name='index'),
)
