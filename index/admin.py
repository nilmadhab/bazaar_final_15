from django.contrib import admin
from index.models import *

class ForexRankingAdmin(admin.ModelAdmin):
    list_display = ('userid', 'balance','equity','rank')

admin.site.register(Users)
admin.site.register(CommodityBase)
admin.site.register(ForexBase)
admin.site.register(Notifications)
admin.site.register(ForexRankings,ForexRankingAdmin)
admin.site.register(Indices)