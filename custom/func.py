from data.models import *
import re
from decimal import Decimal
from datetime import datetime
from django.utils import timezone, formats
from django.utils.dateformat import DateFormat
import json
import urllib2, urllib
from math import exp, log
from random import randint
import time

def current_price(stock_name):
    try:
        req = StocksRate.objects.get(stock = stock_name)
        pr = req.price
    except:
        pr = 0
    return pr


def brockerage_fun(user, price):
    print price
    num = WudClosedOptions.objects.filter(userid=user).count()
    if num < 100:
        br = 0.05*price/100
        if br < 0.02:
            br = 0.02
    elif num>100 and num<1000:
        br = 0.1*price/100
        if br<0.05:
            br = 0.05
    else:
        br = 0.25*(price)/100
        if br<0.1:
            br = 0.1
    print "br = %s" % str(br)
    return br

def valid_quantity(quantity):
    if re.search("^[0-9]+$", quantity) :
        return True
    else:
        return False

def valid_pp(pendingprice):
    if (re.search('^[0-9\.]+$', pendingprice)):
        return True
    else: 
        return False

def timenow():
    time=datetime.now()
    time=time
    tf=DateFormat(time)
    time=tf.format("Y-m-d H:i:s")
    array={'dateString': str(time)}
    return json.dumps(array)

def roundOff(price):
    price = round(price,2)
    lastdigit = round((price*100),0)%10
    
    if(lastdigit < 0):
        lastdigit = -lastdigit
    if(lastdigit == 0):
        add = 0.00
    elif(lastdigit == 1):
        add = -0.01
    elif(lastdigit == 2):
        add = -0.02
    elif(lastdigit == 3):
        add = 0.02
    elif(lastdigit == 4):
        add = 0.01
    elif(lastdigit == 5):
        add = 0.00
    elif(lastdigit == 6):
        add = -0.01
    elif(lastdigit == 7):
        add = -0.02
    elif(lastdigit == 8):
        add = 0.02
    elif(lastdigit == 9):
        add = 0.01
    
    if(price > 0):
        price = price+add
    else:
        price = price-add
    
    return price


def find_price(stock):
    
    stockSplit = stock.split('.')
    name = stockSplit[0]
    que = StocksRate.objects.get(stock=name)
    price = que.price
    print(price)
    
    return price

def dailyPremium(name, count, basePremium, corp):
    que = OptionsBase.objects.get(javaname=name)
    
    i=0
    m = que.randm
    mChunk = m.split('~')
    randArray = []
    
    for x in xrange(0,11):
        if(corp == 'Call'):
            try:
                randArray[x] = mChunk[x]
            except IndexError:
                randArray.append(mChunk[x])
        else:
            try:
                randArray[x] = mChunk[10-x]
            except IndexError:
                randArray.append(mChunk[10-x])
    
    nameChunk = name.split('.')
    nameNew = nameChunk[0]
    
    que2 = StocksRate.objects.get(stock=nameNew)
    
    uPrice = que2.price
    print('Current Price : '+str(uPrice))
    
    diff = que2.diffmin
    flag = 1
    if(diff > 0):
        if(corp == 'Call'):
            flag = 1
        else:
            flag = -1
    else:
        if(corp == 'Put'):
            flag = -1
        else:
            flag = 1
    
    print('diffmin : '+str(diff)+' and basePremium : '+str(basePremium))
    print(randArray[count])
    
    premiumDiff = round( (float(randArray[count])*flag*float(diff)*float(basePremium)/float(uPrice)), 2)
    premiumDiff = roundOff(premiumDiff)
    print('Premium Diff : '+str(premiumDiff))
    
    return premiumDiff
        

def priceRequest(data, options=[]):
    i=0
    result = []
    for d in data:
        try:
            try:
                d['post'] = urllib.urlencode(d['post'])
                request = urllib2.Request(d['url'], d['post'])
                response = urllib2.urlopen(request)
            except:
                response = urllib2.urlopen(d['url'])
        except:
            response = urllib2.urlopen(d)
        print response.info()
        result[i] = response.read()
        response.close()
        i=i+1

    return result

def numbr(pr):
    if(pr.find('.')< pr.find(',')):
        pr = pr.replace('.','')
        pr = pr.translate(str.maketrans(',', '.'))
    else:
        pr = pr.replace(',','')

    return float(pr)


########################################FUNCTIONS FOR WEEKLYVOLATILITY##################################### 

def volatility_fun(high, low):
    max_bi = max(high[5],high[6],high[7],high[8],high[9],high[10],high[11],high[12],high[13],high[14])
    min_bi = min(low[5],low[6],low[7],low[8],low[9],low[10],low[11],low[12],low[13],low[14])

    volal = 2*(float(max_bi)-float(min_bi))/(float(max_bi)+float(min_bi))
    print('returning volal is '+str(volal))
    return volal

def average(arr, front, end):
    avg = 0
    for x in xrange(front,end):
        avg += arr[x]
    avg = avg/(end-front)
    print('average returning '+str(avg))
    return avg

def strike_price(up, volal):
    temp = (float(volal))*float(up)
    sp = []
    for x in xrange(0,11):
        spx = (float(up)+((x-5)/4*temp))
        spx = (vol_roundoff(spx))
        sp.append(str(spx))
    print('strike_price returning '+'~'.join(sp))
    return '~'.join(sp)

def randombet(num1,num2):
    from random import uniform

    num1 = int(num1*100)
    num2 = int(num2*100)
    num = uniform(num1,num2)
    num = num/100
    num = vol_roundoff(num)
    print('randombet returning '+str(num))
    return num

def vol_roundoff(num):
    num = num*1000
    num = int(num)
    temp = num%50
    if(temp<25):
        num = num-temp
    else:
        num = num-temp+50
    num = num/1000
    print('vol_roundoff returning '+str(num))
    return num

def callprice(zp,up,volal,arr):
    callbveek = []
    
    zp = zp.split('~')
    for x in xrange(0,5):
        temp1 = float(float(up)-float(zp[x]))+float(((1.2+float(volal))*float(zp[x]))/43)
        temp2 = float(float(up)-float(zp[x]))+float(((1.3+float(volal))*float(zp[x]))/43)

        callbveek.append(randombet(temp1,temp2))
    print('callprice print 1')

    if(callbveek[4]<1.6):
        i=0
        callbveek[i]=randombet(4,5)
        i=i+1
        callbveek[i]=randombet(3.2,4)
        i=i+1
        callbveek[i]=randombet(2.5,3.2)
        i=i+1
        callbveek[i]=randombet(2,2.5)
        i=i+1
        callbveek[i]=randombet(1.6,2)

    print('callprice print 2')
    bpbweek = callbveek[4]
    for x in xrange(5,10):
        temp1 = exp(log(bpbweek)*(10-x)/6)
        temp2 = exp(log(bpbweek)*(10-x-1)/6)
        callbveek.append(randombet(temp1,temp2))
    print('callprice print 3')
    temp1=1
    temp2 = 0.2
    callbveek[x] = randombet(temp1,temp2)

    callbveek = '~'.join(str(x) for x in callbveek)

    q1 = OptionsBase.objects.get(stock=arr.stock)
    q1.callbpbiweek = callbveek
    q1.currentcall2 = callbveek
    q1.save()
    print('callprice print 5')
def putprice(zp,up,volal,arr):
    zp = zp.split('~')
    putbveek1 = []
    putbveek = []
    for x in xrange(6,11):
        temp1 = (float(zp[x])-float(up))+(((1.2+float(volal))*float(zp[x]))/43)
        temp2 = (float(zp[x])-float(up))+(((1.3+float(volal))*float(zp[x]))/43)

        putbveek1.append(randombet(temp1,temp2))
    print('putprice print 1')

    if(putbveek1[0]<1.6):
        i = 0
        putbveek1[i] = randombet(1.6,2.25)
        i=i+1
        putbveek1[i] = randombet(2.25,3)
        i=i+1
        putbveek1[i] = randombet(3,4)
        i=i+1
        putbveek1[i] = randombet(4,5.25)
        i=i+1
        putbveek1[i] = randombet(5.25,6.75)

    bpbweek = putbveek1[0]
    print('putprice print 2')

    i=0
    temp1=1
    temp2=0.2
    putbveek.append(randombet(temp1,temp2))

    for x in xrange(1,6):
        temp1 = exp(log(bpbweek)*x/6)
        temp2 = exp(log(bpbweek)*(x-1)/6)
        try:
            putbveek[x] = (randombet(temp2,temp1))
        except IndexError:
            putbveek.append(randombet(temp2,temp1))            
    print('putprice print 3')
    putbveek = '~'.join(str(x) for x in putbveek)
    putbveek1 = '~'.join(str(x) for x in putbveek1)

    putbveek=putbveek+'~'+putbveek1
    print('putprice print 4')
    q1 = OptionsBase.objects.get(stock=arr.stock)
    q1.putbpbiweek = putbveek
    q1.currentput2 = putbveek
    q1.save()
    print('putprice print 5')

def volatility_main():
    print('volatility main')
    que = OptionsBase.objects.all()
    for arr in que:
        high = arr.high
        high = high.split('~')
        low = arr.low
        low = low.split('~')
        print('values OK')

        volal = volatility_fun(high,low)

        finalvolatility = volal
        q = OptionsBase.objects.get(stock=arr.stock)
        q.volatility = finalvolatility
        q.save()
        print('updated in optionsbase')
        up = arr.uprice
        sp = strike_price(up,volal)

        q2 = OptionsBase.objects.get(stock=arr.stock)
        q2.strikeprice = sp
        q2.volatility = finalvolatility
        q2.save()
        print('updated again')

        callprice(sp,up,volal,arr)
        putprice(sp,up,volal,arr)


###########################################FUNCTIONS FOR WEEKLY VOLATILITY##########################################################

def startPrice(stock,week):
    stockChunk = stock.split('.')
    stockNew = stockChunk[0]

    que1 =  StocksRate.objects.get(stock=stockNew)
    diff = que1.diff
    uPrice = que1.price
    monday = que1.lastmonday
    friday = que1.lastfriday

    if(diff>0):
        flag = 1
    else:
        flag = -1

    randomBCP=0 + (1.04 * randint(31000, 32000)/32000)

    startPrice = round(uPrice+(flag*randomBCP*diff),2)
    return startPrice
            

def contractprice(stock,week):
    que = FuturesBase.objects.get(javaname=stock)
    start = que.contractprice2
    stockChunk = stock.split('.')
    stockNew = stockChunk[0]

    que1 = StocksRate.objects.get(stock=stockNew)
    uPrice = que1.price
    monday = que1.lastmonday
    friday = que1.lastfriday
    difference = que1.diffmin

    if (uPrice >=0 and uPrice<50):
        randomBCP=0 + (1.10857 * randint(28000,32000)/32000)
    elif(uPrice>=50 and uPrice<100):
        randomBCP=0 + (1.1849 * randint(28000,32000)/32000)
    elif(uPrice>=100 and uPrice<150):
        randomBCP=0 + (1.2626 * randint(28000,32000)/32000)
    elif(uPrice>=150 and uPrice<250):
        randomBCP=0 + (1.0944 * randint(28000,32000)/32000)
    elif(uPrice>=250 and uPrice<500):
        randomBCP=0 + (1.10537 * randint(28000,32000)/32000)
    elif(uPrice>=500 and uPrice<1000):
        randomBCP=0 + (1.1024 * randint(28000,32000)/32000)
    elif(uPrice>=1000 and uPrice<1600):
        randomBCP=0 + (1.1257 * randint(28000,32000)/32000)
    elif(uPrice>=1600):
        randomBCP=0 + (1.1255 * randint(30000,32000)/32000)

    add = difference*randomBCP
    contractPrice = start+add
    if(contractPrice>1.05*uPrice):
        contractPrice = start-add
    admission = Status.objects.all()

    for ob in admission:
        status_now = ob.status
    if(status_now == 0):
        day = datetime.date.today().weekday()
        if((day != 4) and (day != 5) and (day != 3)):
            if(contractPrice > 1.05*uPrice):
                contractPrice = 1.05*uPrice
            elif(contractPrice < 0.95*uPrice):
                contractPrice = 0.95*uPrice
        if(day == 4):
            hour = time.strftime('%H',time.localtime())
            if(int(hour)<14):
                if(contractPrice > 1.045*uPrice):
                    contractPrice = 1.045*uPrice
                elif(contractPrice < 0.955*uPrice):
                    contractPrice = 0.955*uPrice
            else:
                if(contractPrice > 1.04*uPrice):
                    contractPrice=1.04*uPrice
                elif(contractPrice < 0.96*uPrice):
                    contractPrice=0.96*uPrice
        elif(day == 5):
            hour = time.strftime('%H',time.localtime())
            if(int(hour)<11):
                if(contractPrice > 1.035*uPrice):
                    contractPrice = 1.035*uPrice
                elif(contractPrice<0.965*uPrice):
                    contractPrice=0.965*uPrice
            elif((hour<12) and (hour>10)):
                if(contractPrice>1.03*uPrice):
                    contractPrice=1.03*uPrice
                elif(contractPrice < 0.97*uPrice):
                    contractPrice=0.97*uPrice
            elif((hour<13)and(hour>11)):
                if(contractPrice > 1.025*uPrice):
                    contractPrice=1.025*uPrice
                elif(contractPrice<0.975*uPrice):
                    contractPrice=0.975*uPrice
            elif((hour<14)and(hour>12)):
                if(contractPrice>1.02*uPrice):
                    contractPrice=1.02*uPrice
                elif(contractPrice<0.98*uPrice):
                    contractPrice=0.98*uPrice
            elif((hour<15)and(hour>13)):
                if(contractPrice>1.015*uPrice):
                    contractPrice=1.015*uPrice
                elif(contractPrice<0.985*uPrice):
                    contractPrice=0.985*uPrice
            elif((hour<16)and(hour>14)):
                if(contractPrice>1.01*uPrice):
                    contractPrice=1.01*uPrice
                elif(contractPrice<0.99*uPrice):
                    contractPrice=0.99*uPrice
            elif((hour<17)and(hour>15)):
                if(contractPrice>1.00*uPrice):
                    contractPrice=1.00*uPrice
                elif(contractPrice<1*uPrice):
                    contractPrice=1*uPrice
    return contractPrice
