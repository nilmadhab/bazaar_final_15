from custom.globals import globalvars
from data.models import StocksRate
from custom.func import *

class sterminal_wud_opened:
    def __init__(self, q):
        stck = q.stockname.split('.')
        stock = stck[0].upper()
        amtobtained = 0
        cp = current_price(stock)
        triggerprice = q.triggerprice
        if triggerprice == 0:
            triggerprice = 'None'
        currentprice = round(cp,2)
        try:
            que2 = StocksRate.objects.get(stock = stock)
            name_now = que2.compstatname
        except:
            name_now = 'not exist'
        if q.bors == 'Buy':
            amtobtained = (currentprice - q.tranprice)*q.quantity
        else:
            amtobtained = (q.tranprice - currentprice)*q.quantity*globalvars.marginfrac
        pr = round(amtobtained,2)
            
        self.stock = stock
        self.name_now = name_now
        self.current_price = currentprice
        self.triggerprice = triggerprice
        self.pr = pr

class sterminal_pend_closed:
    def __init__(self, q):
        stck = q.stockname.split('.')
        stock = stck[0].lower()
        currentprice = round(current_price(stock),2)
            
        self.current_price = currentprice

class sterminal_pending:
    def __init__(self, q):
        stck = q.stockname.split('.')
        stock = stck[0].lower()
        currentprice = round(current_price(stock),2)
            
        self.current_price = currentprice

