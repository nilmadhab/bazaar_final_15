from django.db import models
from django import forms
#from __future__ import unicode_literals
#This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
    
class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=80L, unique=True)
    class Meta:
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')
    class Meta:
        db_table = 'auth_group_permissions'

class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50L)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100L)
    class Meta:
        db_table = 'auth_permission'

class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=128L)
    last_login = models.DateTimeField()
    is_superuser = models.IntegerField()
    username = models.CharField(max_length=30L, unique=True)
    first_name = models.CharField(max_length=30L)
    last_name = models.CharField(max_length=30L)
    email = models.CharField(max_length=75L)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()
    class Meta:
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)
    class Meta:
        db_table = 'auth_user_groups'

class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)
    class Meta:
        db_table = 'auth_user_user_permissions'


class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    user = models.ForeignKey(AuthUser)
    content_type = models.ForeignKey('DjangoContentType', null=True, blank=True)
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200L)
    action_flag = models.IntegerField()
    change_message = models.TextField()
    class Meta:
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100L)
    app_label = models.CharField(max_length=100L)
    model = models.CharField(max_length=100L)
    class Meta:
        db_table = 'django_content_type'

class DjangoSession(models.Model):
    session_key = models.CharField(max_length=40L, primary_key=True)
    session_data = models.TextField()
    expire_date = models.DateTimeField()
    class Meta:
        db_table = 'django_session'

class DjangoSite(models.Model):
    id = models.IntegerField(primary_key=True)
    domain = models.CharField(max_length=100L)
    name = models.CharField(max_length=50L)
    class Meta:
        db_table = 'django_site'


class FuturesBase(models.Model):
    stockid = models.IntegerField(primary_key=True, db_column='stockId') # Field name made lowercase.
    stock = models.CharField(max_length=200L)
    javaname = models.CharField(max_length=65L, db_column='javaName') # Field name made lowercase.
    closeprice = models.FloatField(db_column='closePrice') # Field name made lowercase.
    startprice2 = models.FloatField(db_column='startPrice2') # Field name made lowercase.
    contractprice2 = models.FloatField(db_column='contractPrice2') # Field name made lowercase.
    diff2 = models.FloatField()
    class Meta:
        db_table = 'futures_base'

class FuturesOpenedOrClosed(models.Model):
    orderid = models.IntegerField(primary_key=True, db_column='orderId') # Field name made lowercase.
    userid = models.CharField(max_length=65L, db_column='userId') # Field name made lowercase.
    stock = models.CharField(max_length=65L)
    quantity = models.IntegerField()
    margin = models.FloatField()
    expiry = models.CharField(max_length=65L)
    expiryno = models.IntegerField(db_column='expiryNo') # Field name made lowercase.
    lors = models.CharField(max_length=65L, db_column='lORs') # Field name made lowercase.
    opencp = models.FloatField(db_column='openCP') # Field name made lowercase.
    profitcp = models.FloatField(db_column='profitCP') # Field name made lowercase.
    time = models.CharField(max_length=65L)
    status = models.IntegerField(default=1)
    triggerprice = models.FloatField(default=0)
    profit = models.FloatField(default=0)
    class Meta:
        db_table = 'futures_opened_or_closed'

class MarketOpen(models.Model):
    oc = models.IntegerField()
    class Meta:
        db_table = 'market_open'

class OptionsBase(models.Model):
    stockid = models.IntegerField()
    stock = models.CharField(max_length=200L)
    javaname = models.CharField(max_length=65L, db_column='javaName') # Field name made lowercase.
    volatility = models.CharField(max_length=200L)
    callbpbiweek = models.CharField(max_length=200L, db_column='CallBPbiweek') # Field name made lowercase.
    putbpbiweek = models.CharField(max_length=200L, db_column='PutBPbiweek') # Field name made lowercase.
    strikeprice = models.CharField(max_length=200L, db_column='StrikePrice') # Field name made lowercase.
    uprice = models.CharField(max_length=200L, db_column='UPrice') # Field name made lowercase.
    high = models.CharField(max_length=400L, db_column='High') # Field name made lowercase.
    low = models.CharField(max_length=400L, db_column='Low') # Field name made lowercase.
    randm = models.CharField(max_length=200L, db_column='randM') # Field name made lowercase.
    lastupdated = models.DateField()
    currentcall2 = models.CharField(max_length=255L, db_column='currentCall2') # Field name made lowercase.
    currentput2 = models.CharField(max_length=255L, db_column='currentPut2') # Field name made lowercase.
    checkcall = models.CharField(max_length=800L, db_column='checkCall') # Field name made lowercase.
    checkput = models.CharField(max_length=800L, db_column='checkPut') # Field name made lowercase.
    class Meta:
        db_table = 'options_base'

class OptionsOpenedOrClosed(models.Model):
    transactionid = models.IntegerField(primary_key=True, db_column='transactionId') # Field name made lowercase.
    userid = models.CharField(max_length=65L, db_column='userId') # Field name made lowercase.
    name = models.CharField(max_length=255L)
    quantity = models.IntegerField()
    expiry = models.CharField(max_length=25L)
    longorshort = models.CharField(max_length=25L, db_column='longORshort') # Field name made lowercase.
    callorput = models.CharField(max_length=25L, db_column='callORput') # Field name made lowercase.
    margin = models.CharField(max_length=25L)
    strikeprice = models.FloatField(db_column='strikePrice') # Field name made lowercase.
    underlyingprice = models.FloatField(db_column='underlyingPrice') # Field name made lowercase.
    premium = models.FloatField()
    profit = models.FloatField(default=0)
    volatility = models.FloatField()
    status = models.FloatField()
    date = models.DateTimeField()
    class Meta:
        db_table = 'options_opened_or_closed'

class RediffLink(models.Model):
    email = models.CharField(max_length=50L, db_column='EMail') # Field name made lowercase.
    sent = models.IntegerField()
    class Meta:
        db_table = 'rediff_link'

class RediffLinks(models.Model):
    link = models.CharField(max_length=500L)
    id = models.IntegerField(primary_key=True)
    class Meta:
        db_table = 'rediff_links'

class Status(models.Model):
    status = models.IntegerField()
    class Meta:
        db_table = 'status'

class Stockprice(models.Model):
    file = models.CharField(max_length=100L)
    name = models.CharField(max_length=100L)
    price = models.FloatField()
    vary = models.CharField(max_length=100L)
    series = models.CharField(max_length=2L)
    class Meta:
        db_table = 'stockprice'

class StocksRate(models.Model):
    stock = models.CharField(max_length=100L)
    compstatname = models.CharField(max_length=25L, db_column='compStatName') # Field name made lowercase.
    price = models.FloatField()
    open = models.FloatField()
    lastmonday = models.FloatField(db_column='lastMonday') # Field name made lowercase.
    lastfriday = models.FloatField(db_column='lastFriday') # Field name made lowercase.
    diff = models.FloatField()
    series = models.CharField(max_length=2L)
    quantity = models.IntegerField()
    lastmin = models.FloatField()
    diffmin = models.FloatField()
    class Meta:
        db_table = 'stocks_rate'

class WudClosedOptions(models.Model):
    userid = models.CharField(max_length=200L)
    orderno = models.BigIntegerField(primary_key=True)
    trandateandtime = models.DateTimeField()
    stockname = models.CharField(max_length=200L)
    bors = models.CharField(max_length=20L)
    quantity = models.BigIntegerField()
    tranprice = models.FloatField()
    brockerage = models.FloatField()
    closecp = models.FloatField(db_column='closeCP') # Field name made lowercase.
    profit = models.FloatField()
    class Meta:
        db_table = 'wud_closed_options'

class WudOpenedOptions(models.Model):
    userid = models.CharField(max_length=200L)
    orderno = models.IntegerField(primary_key=True)
    trandateandtime = models.DateTimeField()
    stockname = models.CharField(max_length=200L)
    bors = models.CharField(max_length=10L, blank=True)
    quantity = models.BigIntegerField()
    tranprice = models.FloatField()
    triggerprice = models.FloatField(default=0)
    margin = models.FloatField(default=0)
    class Meta:
        db_table = 'wud_opened_options'

class WudPendingCloseOptions(models.Model):
    userid = models.CharField(max_length=200L)
    orderno = models.BigIntegerField(primary_key=True)
    trandateandtime = models.DateTimeField()
    stockname = models.CharField(max_length=200L)
    bors = models.CharField(max_length=10L)
    quantity = models.BigIntegerField()
    pendingprice = models.FloatField()
    time = models.DateTimeField()
    tranprice = models.FloatField(db_column='tranPrice') # Field name made lowercase.
    margin = models.FloatField(default=0)
    class Meta:
        db_table = 'wud_pending_close_options'

class WudPendingOptions(models.Model):
    pendingid = models.BigIntegerField(primary_key=True)
    userid = models.CharField(max_length=200L)
    stockname = models.CharField(max_length=200L)
    quantity = models.BigIntegerField()
    bors = models.CharField(max_length=10L)
    pendingprice = models.FloatField()
    comment = models.CharField(max_length=200L)
    time = models.DateTimeField()
    triggerprice = models.FloatField()
    class Meta:
        db_table = 'wud_pending_options'

class WudRankings(models.Model):
    userid = models.CharField(max_length=200L, primary_key=True)
    cashbalance = models.FloatField()
    reserve = models.FloatField()
    equity = models.FloatField()
    rank = models.IntegerField()
    margin = models.FloatField()
    class Meta:
        db_table = 'wud_rankings'

class WudUserDeductions(models.Model):
    userid = models.CharField(max_length=50L)
    orderno = models.IntegerField()
    bal_ded = models.FloatField()
    worth_ded = models.FloatField()
    seq_no = models.IntegerField(primary_key=True, db_column='Seq_no') # Field name made lowercase.
    class Meta:
        db_table = 'wud_user_deductions'

