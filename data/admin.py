from django.contrib import admin
from data.models import *

admin.site.register(WudRankings)
admin.site.register(FuturesBase)
admin.site.register(OptionsBase)
admin.site.register(StocksRate)
admin.site.register(MarketOpen)