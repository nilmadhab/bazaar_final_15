'''
Created on Jun 8, 2013

@author: Srinivasan
'''
class AlgoTradeRouter(object): 
    def db_for_read(self, model, **hints):
        "Point all operations on algotrade models to 'algotrade' database"
        if model._meta.app_label == 'algotrade':
            return 'algotrade'
        return None

    def db_for_write(self, model, **hints):
        "Point all operations on algotrade models to 'algotrade'"
        if model._meta.app_label == 'algotrade':
            return 'algotrade'
        return None
    
    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in algotrade app"
        if obj1._meta.app_label == 'algotrade' and obj2._meta.app_label == 'algotrade':
            return True
        elif 'algotrade' not in [obj1._meta.app_label, obj2._meta.app_label]: 
            return True
        return None
    
    def allow_syncdb(self, db, model):
        if db == 'algotrade' and model._meta.app_label == "algotrade":
            return True
        elif db == 'algotrade' or model._meta.app_label == "algotrade":
            return False
        else:
            return None