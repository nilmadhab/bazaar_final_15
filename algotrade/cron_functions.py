from algotrade.models import ForexPendingOptions, ForexRankings, \
    ForexOpenedOptions, CommPendingOptions, CommOpenedOptions, CommClosedOptions, \
    ForexUserDeductions, ForexClosedOptions
from index.utils import current_price, calc_profit, find_price
from django.utils import timezone
import datetime
def forex_pending():
    result=ForexPendingOptions.objects.all()
    for arr in result:
        userid=arr.userid
        currentprice=current_price(arr.currency,arr.bors)
        triggerprice=arr.triggerprice
        flag=0
        if arr.bors=='Long':
            flag=arr.pendingprice-currentprice
        else:
            flag=currentprice-arr.pendingprice 
        if flag>=0:
            currency=arr.currency
            levearage=arr.levearage
            bors=arr.bors
            lots=arr.lots
            margin=(levearage*lots*10)
            openprice = arr.pendingprice
            closepr=current_price(currency,bors)
            pl=calc_profit(levearage,lots,openprice,currency,bors)
            amountnow = (openprice*lots);
            ranking=ForexRankings.objects.get(userid=userid)
            ranking.balance=ranking.balance-amountnow
            ranking.equity=ranking.equity-pl
            ranking.margin=ranking.margin-(openprice*lots*levearage*10)
            ranking.save()
            try:
                opened=ForexOpenedOptions(userid=userid,opendateandtime=timezone.now(),currency=currency,levearage=levearage,bors=bors,lots=lots,openprice=currentprice,triggerprice=triggerprice)
                opened.save()
                ranking.margin=ranking.margin+margin
                ranking.save()
                print("Your transaction has been successfully completed.")
                arr.delete()
            except:
                print("error")

def comm_pending():    
    marginfrac = 0.5
    result=CommPendingOptions.objects.all()
    for arr1 in result:
        currentprice=find_price(arr1.tradename)    
        if currentprice=="":
            print "Price cannot be Zer000000"
            return
        quantity=arr1.quantity
        userid = arr1.userid
        tr = arr1.pendingprice
        triggerprice=arr1.triggerprice
        req=0    
        quan=0    
        stockname=arr1.tradename    
        bors=arr1.bors    
        levearage=arr1.levearage
        flag=0    
        if bors=='Buy':    
            if currentprice<=arr1.pendingprice:    
                flag=1    
        else:    
            if currentprice>=arr1.pendingprice:
                flag=1   
        if flag==1:
            print(arr1.pendingid)
            ranking=ForexRankings.objects.get(userid=userid)
            if bors=='Long':     
                req=quantity*tr*levearage
                if ranking.balance<req:
                    quan=int(ranking.balance/currentprice)
                    print("You can only buy <b>"+str(quan)+"</b> stocks<br>")
                    arr1.delete()
                else:
                    ranking.balance=ranking.balance-req
                    ranking.save()
                    re1 = (-1)*req;
                    profit1 = 0
                    opened=CommOpenedOptions.objects.create(userid=userid,tradename=stockname,opendateandtime=timezone.now(),bors=bors,quantity=quantity,openprice=tr,levearage=levearage,triggerprice=triggerprice) 
                    print("You have successfuly bought <b>"+str(quantity)+"</b> stock holdings.<br>")
                    arr1.delete()
            else:
                req=quantity*tr*levearage
                if arr1.balance<req:
                    quan=int(arr1.balance/(currentprice*levearage))
                    print("You can only short sell <b>"+str(quan)+"</b> stocks<br>");
                else:
                    ranking.balance=ranking.balance-req
                    ranking.save()            
                    re1 = (-1)*req
                    profit1 = 0
                    try:
                        opened=CommOpenedOptions(userid=userid,tradename=stockname,opendateandtime=timezone.now(),bors=bors,quantity=quantity,openprice=tr,levearage=levearage,triggerprice=triggerprice)
                        opened.save();
                        print("You have successfuly short sold <b>"+str(quantity)+"</b> stock holdings.<br>");
                        arr1.delete()
                    except:
                        print("Error")
                print("Open orders file called<br>");


def comm_trigclose():
    check_r=CommOpenedOptions.objects.all()
    check_cnt=check_r.count()
    if not check_r or check_cnt==0:
        print 'No users exist'
    
    for check_a in check_r:
        orderno = check_a.orderno
        userid = check_a.userid
        stockname=check_a.tradename
        oldquantity=check_a.quantity
        bors = check_a.bors
        cP = check_a.openprice
        sellprice=find_price(stockname)
        amount=oldquantity*sellprice;
        triggerprice = check_a.triggerprice
        trigcheck = 0
        if triggerprice==0:
            pass
        else:
            if check_a.bors=="Buy":
                if sellprice < triggerprice:
                    trigcheck =1
                    amount = oldquantity*(cP-triggerprice)
            else:
                if sellprice > triggerprice:
                    trigcheck = 1
                    amount = oldquantity*(triggerprice-cP)
                      
        ranking=ForexRankings.objects.get(userid=userid)
        if trigcheck == 1:
            if bors == "Long":
                payoff = triggerprice*oldquantity
                ranking.equity=ranking.equity-amount
                ranking.balance=ranking.balance-payoff
                ranking.save()
                w = amount*(-1)
                closed=CommClosedOptions.objects.create(userid=userid,trandateandtime=timezone.now(),tradename=stockname,bors='Short',quantity=oldquantity,tranprice=triggerprice)
                check_a.delete()
    
                output="Deleted "
                output+=userid
                output+="'s order of "
                output+=stockname
                output+="<br/>"
            elif bors=="Short":
                payoff = triggerprice*oldquantity
                ranking.equity=ranking.equity-amount
                ranking.balance=ranking.balance-payoff
                ranking.save()
                w = amount*(-1)
                closed=CommClosedOptions.objects.create(userid=userid,trandateandtime=timezone.now(),tradename=stockname,bors='Long',quantity=oldquantity,tranprice=sellprice)
                check_a.delete()
                output="Deleted "
                output+=userid
                output+="'s order of "
                output+=stockname
                output+="<br/>"
            print output
            
def forex_trigclose():
    from index.models import ForexOpenedOptions
    from index.utils import current_price,calc_profit_trigger 
    check_r=ForexOpenedOptions.objects.all()
    check_count=ForexOpenedOptions.objects.count()
    if not check_r or check_count==0:
        return
    count=0
    for check_a in check_r:
        userid = check_a.userid;
        currency = check_a.currency.strip(' \t\n\r')
        levearage = check_a.levearage
        lots = check_a.lots;
        orderno = check_a.orderno;
        triggerprice=check_a.triggerprice;
        cP = check_a.openprice
        bors = check_a.bors
        flag = 0
        if triggerprice==0:
            pass
        else:
            if bors=="Short":
                bs="Long";
            else:
                bs="Short"
            
            closingpr=current_price(currency,bs)
            print(closingpr)
            if bors=="Long":
                if triggerprice>=closingpr:
                    flag = 1
            else:
                if closingpr>=triggerprice:
                    flag = 1
                    
            if flag==1:
                try:
                    porl=calc_profit_trigger(levearage,lots,cP,bors,triggerprice)
                    count=count+1
                    user_data=ForexRankings.objects.get(userid=userid)
                    user_data.balance=user_data.balance+porl
                    user_data.equity=user_data.equity+porl
                    user_data.margin=user_data.margin-(cP*lots*levearage*10)                
                    
                    user_deduct=ForexUserDeductions()
                    user_deduct.userid=userid
                    user_deduct.orderno=orderno
                    user_deduct.bal_ded=porl
                    user_deduct.worth_ded=porl
                    
                    user_closed=ForexClosedOptions()
                    user_closed.userid=userid
                    user_closed.closedateandtime=datetime.datetime.now()
                    user_closed.currency=currency
                    user_closed.lots=lots
                    user_closed.bors=bors
                    user_closed.openprice=cP
                    user_closed.levearage=levearage
                    user_closed.fltprofit=porl
                    user_closed.closeprice=triggerprice
                    user_closed.opendateandtime=check_a['opendateandtime']
                    
                    user_data.save()
                    user_deduct.save()
                    user_closed.save()
                    
                    user_opened=ForexOpenedOptions.objects.get(orderno=orderno)
                    if user_opened.userid==userid:
                        user_opened.delete()
                    
                except:
                    print('Rollback needed')
                    #Roll back Here ## IMPORTANT
    
                if porl>0:
                    print('Transaction Closed Successfully with profit of Rs '+porl+' of '+userid)
                else:
                    print('Transaction Closed Successfully with loss of Rs '+(-porl)+' of '+userid)   
    
    print(str(count)+' orders were closed')    
    
    
def updateforexranks():
    results=ForexRankings.objects.order_by('-equity')
    rank=0
    for row in results:
        rank=rank+1
        row.rank=rank
        row.save()
    print('Ranks Updated')
    