from __future__ import unicode_literals
from django.db import models

class Users(models.Model):
    username = models.CharField(db_column='Username', primary_key=True, max_length=50) # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=50) # Field name made lowercase.
    class Meta:
        db_table = 'users'

# Forex Models


class CommClosedOptions(models.Model):
    serial = models.AutoField(primary_key=True)
    userid = models.CharField(max_length=75)
    trandateandtime = models.DateTimeField()
    tradename = models.CharField(max_length=100)
    bors = models.CharField(max_length=15)
    quantity = models.IntegerField()
    tranprice = models.FloatField()
    brockerage = models.FloatField()
    levearage = models.IntegerField()
    closecp = models.FloatField(db_column='closeCP') # Field name made lowercase.
    profit = models.FloatField()
    class Meta:
        db_table = 'comm_closed_options'

class CommOpenedOptions(models.Model):
    userid = models.CharField(db_column='userId', max_length=75) # Field name made lowercase.
    orderno = models.AutoField(db_column='orderNo', primary_key=True) # Field name made lowercase.
    opendateandtime = models.DateTimeField()
    tradename = models.CharField(max_length=100)
    quantity = models.IntegerField()
    bors = models.CharField(max_length=25)
    openprice = models.FloatField(db_column='openPrice') # Field name made lowercase.
    triggerprice = models.FloatField()
    levearage = models.IntegerField()
    class Meta:
        db_table = 'comm_opened_options'

class CommPendingCloseOptions(models.Model):
    userid = models.CharField(db_column='userId', max_length=100) # Field name made lowercase.
    orderid = models.AutoField(db_column='orderId', primary_key=True) # Field name made lowercase.
    trandateandtime = models.DateTimeField()
    tradename = models.CharField(max_length=100)
    bors = models.CharField(max_length=25)
    quantity = models.IntegerField()
    pendingprice = models.FloatField(db_column='pendingPrice') # Field name made lowercase.
    levearage = models.IntegerField()
    triggerprice = models.FloatField(db_column='triggerPrice') # Field name made lowercase.
    tranprice = models.FloatField(db_column='tranPrice') # Field name made lowercase.
    class Meta:
        db_table = 'comm_pending_close_options'

class CommPendingOptions(models.Model):
    pendingid = models.AutoField(db_column='pendingId', primary_key=True) # Field name made lowercase.
    userid = models.CharField(db_column='userId', max_length=100) # Field name made lowercase.
    tradename = models.CharField(max_length=100)
    quantity = models.IntegerField()
    bors = models.CharField(max_length=10)
    pendingprice = models.FloatField(db_column='pendingPrice') # Field name made lowercase.
    triggerprice = models.FloatField(db_column='triggerPrice') # Field name made lowercase.
    levearage = models.IntegerField()
    class Meta:
        db_table = 'comm_pending_options'

class CommodityBase(models.Model):
    name=models.CharField(max_length=100)
    commodity = models.CharField(max_length=100)
    price = models.FloatField()
    tradename = models.CharField(max_length=100)
    open = models.FloatField()
    identifier=models.CharField(max_length=10)
    class Meta:
        db_table = 'commodity_base'

class CommodityTrading(models.Model):
    commodity = models.CharField(max_length=75)
    tradename = models.CharField(max_length=50)
    price = models.FloatField()
    graph_id = models.IntegerField()
    open = models.FloatField()
    newname = models.CharField(db_column='newName', max_length=100) # Field name made lowercase.
    class Meta:
        db_table = 'commodity_trading'

class ForexBase(models.Model):
    currency = models.CharField(max_length=50,unique=True)
    bid = models.FloatField()
    ask = models.FloatField()
    bidvar = models.CharField(max_length=50)
    askvar = models.CharField(max_length=50)
    open = models.FloatField()
    identifier=models.CharField(max_length=10)
    def __unicode__(self):
        return self.currency
    class Meta:
        db_table = 'forex_base'

class ForexClosedOptions(models.Model):
    userid = models.TextField()
    orderno = models.AutoField(primary_key=True)
    opendateandtime = models.DateTimeField()
    currency = models.TextField()
    levearage = models.IntegerField()
    bors = models.CharField(max_length=10, blank=True)
    lots = models.IntegerField()
    openprice = models.FloatField()
    closeprice = models.FloatField()
    fltprofit = models.FloatField()
    closedateandtime = models.DateTimeField()
    fltstorage = models.FloatField(blank=True, null=True)
    class Meta:
        db_table = 'forex_closed_options'
        
class Indices(models.Model):
    index=models.CharField(max_length=100)
    value=models.FloatField()
    change=models.FloatField()
    percent=models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.index
    class Meta:
        db_table='indices'

class ForexOpenedOptions(models.Model):
    userid = models.TextField()
    orderno = models.AutoField(primary_key=True)
    opendateandtime = models.DateTimeField()
    currency = models.TextField()
    levearage = models.IntegerField()
    bors = models.CharField(max_length=10, blank=True)
    lots = models.IntegerField()
    openprice = models.FloatField()
    triggerprice = models.FloatField(db_column='triggerPrice') # Field name made lowercase.
    class Meta:
        db_table = 'forex_opened_options'

class ForexPendingOptions(models.Model):
    pendingid = models.AutoField(primary_key=True)
    userid = models.TextField()
    currency = models.TextField()
    lots = models.IntegerField()
    levearage = models.IntegerField()
    bors = models.CharField(max_length=10)
    pendingprice = models.FloatField()
    triggerprice = models.FloatField(db_column='triggerPrice') # Field name made lowercase.
    class Meta:
        db_table = 'forex_pending_options'

class ForexRankings(models.Model):
    userid = models.CharField(primary_key=True, max_length=50)
    balance = models.FloatField()
    equity = models.FloatField()
    margin = models.FloatField()
    percentage = models.FloatField()
    rank = models.IntegerField()
    ex_bal = models.FloatField()
    ex_wor = models.FloatField()
    class Meta:
        db_table = 'forex_rankings'

class ForexUserDeductions(models.Model):
    seq_no = models.AutoField(db_column='Seq_no', primary_key=True) # Field name made lowercase.
    userid = models.CharField(max_length=55)
    orderno = models.IntegerField()
    bal_ded = models.FloatField()
    worth_ded = models.FloatField()
    class Meta:
        db_table = 'forex_user_deductions'

class Forexbasedata(models.Model):
    commodityid = models.IntegerField(db_column='commodityId', primary_key=True) # Field name made lowercase.
    country = models.CharField(max_length=65)
    commodity = models.CharField(max_length=65)
    class Meta:
        db_table = 'forexbasedata'

# End of Forex Models

# Woodstock Models

class FuturesBase(models.Model):
    stockid = models.IntegerField(db_column='stockId', primary_key=True) # Field name made lowercase.
    stock = models.CharField(max_length=200)
    javaname = models.CharField(db_column='javaName', max_length=65) # Field name made lowercase.
    closeprice = models.FloatField(db_column='closePrice') # Field name made lowercase.
    startprice2 = models.FloatField(db_column='startPrice2') # Field name made lowercase.
    contractprice2 = models.FloatField(db_column='contractPrice2') # Field name made lowercase.
    diff2 = models.FloatField()
    class Meta:
        db_table = 'futures_base'

class FuturesOpenedOrClosed(models.Model):
    orderid = models.AutoField(db_column='orderId', primary_key=True) # Field name made lowercase.
    userid = models.CharField(db_column='userId', max_length=65) # Field name made lowercase.
    stock = models.CharField(max_length=65)
    quantity = models.IntegerField()
    margin = models.FloatField()
    expiry = models.CharField(max_length=65)
    expiryno = models.IntegerField(db_column='expiryNo') # Field name made lowercase.
    lors = models.CharField(db_column='lORs', max_length=65) # Field name made lowercase.
    opencp = models.FloatField(db_column='openCP') # Field name made lowercase.
    profitcp = models.FloatField(db_column='profitCP') # Field name made lowercase.
    time = models.CharField(max_length=65)
    status = models.IntegerField()
    triggerprice = models.FloatField()
    profit = models.FloatField()
    class Meta:
        db_table = 'futures_opened_or_closed'

class MarketOpen(models.Model):
    oc = models.IntegerField()
    class Meta:
        db_table = 'market_open'

class OptionsBase(models.Model):
    stockid = models.IntegerField()
    stock = models.CharField(max_length=200)
    javaname = models.CharField(db_column='javaName', max_length=65) # Field name made lowercase.
    volatility = models.CharField(max_length=200)
    callbpbiweek = models.CharField(db_column='CallBPbiweek', max_length=200) # Field name made lowercase.
    putbpbiweek = models.CharField(db_column='PutBPbiweek', max_length=200) # Field name made lowercase.
    strikeprice = models.CharField(db_column='StrikePrice', max_length=200) # Field name made lowercase.
    uprice = models.FloatField(db_column='UPrice') # Field name made lowercase.
    high = models.CharField(db_column='High', max_length=400) # Field name made lowercase.
    low = models.CharField(db_column='Low', max_length=400) # Field name made lowercase.
    randm = models.CharField(db_column='randM', max_length=200) # Field name made lowercase.
    lastupdated = models.DateField()
    currentcall2 = models.CharField(db_column='currentCall2', max_length=255) # Field name made lowercase.
    currentput2 = models.CharField(db_column='currentPut2', max_length=255) # Field name made lowercase.
    checkcall = models.CharField(db_column='checkCall', max_length=800) # Field name made lowercase.
    checkput = models.CharField(db_column='checkPut', max_length=800) # Field name made lowercase.
    class Meta:
        db_table = 'options_base'

class OptionsOpenedOrClosed(models.Model):
    transactionid = models.AutoField(db_column='transactionId', primary_key=True) # Field name made lowercase.
    userid = models.CharField(db_column='userId', max_length=65) # Field name made lowercase.
    name = models.CharField(max_length=255)
    quantity = models.IntegerField()
    expiry = models.CharField(max_length=25)
    longorshort = models.CharField(db_column='longORshort', max_length=25) # Field name made lowercase.
    callorput = models.CharField(db_column='callORput', max_length=25) # Field name made lowercase.
    margin = models.FloatField()
    strikeprice = models.FloatField(db_column='strikePrice') # Field name made lowercase.
    underlyingprice = models.FloatField(db_column='underlyingPrice') # Field name made lowercase.
    premium = models.FloatField()
    profit = models.FloatField()
    volatility = models.FloatField()
    status = models.FloatField()
    date = models.DateTimeField()
    class Meta:
        db_table = 'options_opened_or_closed'

class RediffLinks(models.Model):
    link = models.CharField(max_length=500)
    id = models.IntegerField(primary_key=True)
    class Meta:
        db_table = 'rediff_links'

class Status(models.Model):
    status = models.IntegerField()
    class Meta:
        db_table = 'status'

class Stockprice(models.Model):
    file = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    price = models.FloatField()
    vary = models.CharField(max_length=100)
    series = models.CharField(max_length=2)
    class Meta:
        db_table = 'stockprice'

class StocksRate(models.Model):
    stock = models.CharField(max_length=100)
    compstatname = models.CharField(db_column='compStatName', max_length=25) # Field name made lowercase.
    price = models.FloatField()
    open = models.FloatField()
    lastmonday = models.FloatField(db_column='lastMonday') # Field name made lowercase.
    lastfriday = models.FloatField(db_column='lastFriday') # Field name made lowercase.
    diff = models.FloatField()
    series = models.CharField(max_length=2)
    quantity = models.IntegerField()
    lastmin = models.FloatField()
    diffmin = models.FloatField()
    class Meta:
        db_table = 'stocks_rate'


class WudClosedOptions(models.Model):
    userid = models.CharField(max_length=200)
    orderno = models.AutoField(primary_key=True)
    trandateandtime = models.DateTimeField()
    stockname = models.CharField(max_length=200)
    bors = models.CharField(max_length=20)
    quantity = models.BigIntegerField()
    tranprice = models.FloatField()
    brockerage = models.FloatField()
    closecp = models.FloatField(db_column='closeCP') # Field name made lowercase.
    profit = models.FloatField()
    class Meta:
        db_table = 'wud_closed_options'

class WudOpenedOptions(models.Model):
    userid = models.CharField(max_length=200)
    orderno = models.AutoField(primary_key=True)
    trandateandtime = models.DateTimeField()
    stockname = models.CharField(max_length=200)
    bors = models.CharField(max_length=10, blank=True)
    quantity = models.BigIntegerField()
    tranprice = models.FloatField()
    triggerprice = models.FloatField()
    margin = models.FloatField()
    class Meta:
        db_table = 'wud_opened_options'

class WudPendingCloseOptions(models.Model):
    userid = models.CharField(max_length=200)
    orderno = models.AutoField(primary_key=True)
    trandateandtime = models.DateTimeField()
    stockname = models.CharField(max_length=200)
    bors = models.CharField(max_length=10)
    quantity = models.BigIntegerField()
    pendingprice = models.FloatField()
    time = models.DateTimeField()
    tranprice = models.FloatField(db_column='tranPrice') # Field name made lowercase.
    margin = models.FloatField()
    class Meta:
        db_table = 'wud_pending_close_options'

class WudPendingOptions(models.Model):
    pendingid = models.AutoField(primary_key=True)
    userid = models.CharField(max_length=200)
    stockname = models.CharField(max_length=200)
    quantity = models.BigIntegerField()
    bors = models.CharField(max_length=10)
    pendingprice = models.FloatField()
    comment = models.CharField(max_length=200)
    time = models.DateTimeField()
    triggerprice = models.FloatField()
    class Meta:
        db_table = 'wud_pending_options'

class WudRankings(models.Model):
    userid = models.CharField(primary_key=True, max_length=200)
    cashbalance = models.FloatField()
    reserve = models.FloatField()
    equity = models.FloatField()
    rank = models.IntegerField()
    margin = models.FloatField()
    class Meta:
        db_table = 'wud_rankings'

class WudUserDeductions(models.Model):
    userid = models.CharField(max_length=50)
    orderno = models.IntegerField()
    bal_ded = models.FloatField()
    worth_ded = models.FloatField()
    seq_no = models.AutoField(db_column='Seq_no', primary_key=True) # Field name made lowercase.
    class Meta:
        db_table = 'wud_user_deductions'
        
class Notifications(models.Model):
    time=models.TimeField()
    message=models.TextField()

# End of Woodstock Links