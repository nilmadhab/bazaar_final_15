from algotrade.forms import OpenCommTransaction, CommPendingTransaction, \
    OpenTransaction, PendingTransaction, CancelOrderForm, CommCancelOrderForm
from algotrade.models import ForexOpenedOptions, ForexRankings, \
    ForexClosedOptions, ForexPendingOptions, CommOpenedOptions, CommClosedOptions, \
    CommPendingOptions
from index.models import ForexBase, CommodityBase
from index.utils import checkparameters, current_price, valid_pp, calc_profit, \
    find_price, calc_comm_profit, brockerage
from datetime import timedelta
from django.http.response import HttpResponse, Http404
from django.utils import timezone
from piston.handler import BaseHandler
from piston.utils import rc, throttle

class Urls(BaseHandler):
    allowed_methods = ('GET')
    model = ForexBase

    def read(self, request):
        """
        Returns a the user Account details containing userid,rank,
        balance, equity and margin
        """
        forex_url='http://download.finance.yahoo.com/d/quotes.csv?s=USDINR=X+USDCAD=X+USDJPY=X+USDGBP=X+USDEUR=X+USDCHF=X+USDAUD=X+USDNZD=X&f=nba';
        comm_url='http://download.finance.yahoo.com/d/quotes.csv?s=GCZ12.CMX+SIF13.CMX+CLZ12.NYM+NGZ12.NYM+CZ12.CBT+CCZ12.NYB+LBF13.CME+SF13.CBT+OZ12.CBT+KCZ12.NYB+CTZ12.NYB+OK13.CBT+PAF13.NYM+CLG13.NYM+CCH13.NYB+KCH13.NYB+CTH13.NYB+GCF13.CMX+CH13.CBT+SH13.CBT&f=nba';
        urls={'forex':forex_url,'commodity':comm_url}
        return urls


class ForexKeys(BaseHandler):
    allowed_methods = ('GET')
    model = ForexBase

    def read(self, request):
        """
        Returns a the user Account details containing userid,rank,
        balance, equity and margin
        """
        base = ForexBase.objects
        return base.all()
    
class CommodityKeys(BaseHandler):
    allowed_methods = ('GET')
    model = CommodityBase

    def read(self, request):
        """
        Returns a the user Account details containing userid,rank,
        balance, equity and margin
        """
        base = CommodityBase.objects
        return base.all()


class Account(BaseHandler):
    allowed_methods = ('GET')
    model = ForexRankings

    def read(self, request):
        """
        Returns a the user Account details containing userid,rank,
        balance, equity and margin
        """
        user = request.user.username
        base = ForexRankings.objects
        return base.get(userid=user)

class ForexOpen(BaseHandler):
    allowed_methods = ('GET', 'POST')
    model = ForexOpenedOptions   

    def read(self, request, orderid=None):
        """
        Returns a single order if `orderid` is given,
        otherwise a subset.
        """
        user = request.user.username
        base = ForexOpenedOptions.objects
        page=0
        try:
            page=request.GET['page']
            currency = request.GET['currency']
        except:
            currency = None
        
        start=int(page)*20
        end=int(start)+20
        
        if orderid:
            return base.get(pk=orderid)
        elif currency:
            return base.filter(userid=user).filter(currency=currency)[start:end]
        else:
            return base.filter(userid=user)[start:end]
         
    @throttle(500, 24*60*60)   
    def create(self, request):
        """
        Creates a new OpenedOption and Saves it. Returns the created Object
        Accepts POST 
        """
        allow = checkparameters(request)
        if allow == 1 or allow == 2:
            lots = int(request.POST['lots'])
            user = request.user.username
            currency = request.POST['currency']
            levearage = int(request.POST['levearage'])
            bors = request.POST['bors']
            pendingprice = request.POST['pendingprice']
            currentprice = current_price(currency, bors)
            triggerprice = request.POST['triggerprice']
            if triggerprice == '' or triggerprice == ' ' or triggerprice < 0:
                triggerprice = '0'
            elif valid_pp(triggerprice):
                triggerprice = float(triggerprice)
        
            if lots < 0 or lots > 50 or lots == "":
                return rc.BAD_REQUEST
            if pendingprice == "":
                result = ForexRankings.objects.get(userid=request.user.username)
                if result:
                    margin = result.margin + (currentprice * levearage * lots * 10)  
                    if currentprice == 0 or currentprice == "" or currentprice < 0:
                        return rc.BAD_REQUEST
                    c = calc_profit(levearage, lots, currentprice, currency, bors) - (lots * currentprice)
                    afterequity = result.balance + calc_profit(levearage, lots, currentprice, currency, bors) - (lots * currentprice)
                    e = calc_profit(levearage, lots, currentprice, currency, bors)
                    equity = result.equity + calc_profit(levearage, lots, currentprice, currency, bors)
                    per = afterequity / margin
                    per = per * 100;
                    balequityflag = 0;
                    if result.balance < 0:
                        return rc.FORBIDDEN
                    if balequityflag == 0:
                        form_array = {'currency':currency, 'levearage':levearage, 'lots':lots, 'bors':bors, 'currentprice':currentprice, 'triggerprice':triggerprice}
                        form_array['userid'] = request.user.username
                        open_form = OpenTransaction(form_array)
                        if open_form.is_valid():
                            open_form.save()
                        else:
                            return rc.BAD_REQUEST
                        result.margin = margin
                        result.balance = afterequity
                        result.equity = equity
                        result.save()
                        return rc.CREATED
                    else:
                        return rc.BAD_REQUEST
                else:
                    if valid_pp(pendingprice):
                        pendingprice = float(pendingprice)
                        form_array = {'currency':currency, 'levearage':levearage, 'lots':lots, 'bors':bors, 'pendingprice':pendingprice, 'triggerprice':triggerprice}
                        form_array['userid'] = user
                        pending_form = PendingTransaction(form_array)
                        if pending_form.is_valid():
                            pending_form.save()
                        else:
                            return rc.BAD_REQUEST
                        return rc.CREATED
                    else:
                        return rc.FORBIDDEN
                        

class ForexClose(BaseHandler):
    allowed_methods = ('GET', 'POST')
    model = ForexClosedOptions  
    
    def read(self, request, orderid=None):
        """
        Returns a single order if `orderid` is given,
        otherwise a subset.
        """
        user = request.user.username
        base = ForexClosedOptions.objects
        page=0
        try:
            page=request.GET['page']
            currency = request.GET['currency']
        except:
            currency = None
            
        start=int(page)*20
        end=int(start)+20
        if orderid:
            return base.get(pk=orderid)
        elif currency:
            return base.filter(userid=user).filter(currency=currency)[start:end]
        else:
            return base.filter(userid=user)[start:end]
        
    @throttle(500, 24*60*60)  
    def create(self, request):
        user = request.user.username
        allow = checkparameters(request)
        if allow == 1 or allow == 2:
            try:
                orderid = request.POST['orderid'];
            except:
                pass
            
            opened = 0;
            try:
                multiple = request.POST['multiple']
            except:
                multiple = None
            
            if not multiple:
                try:
                    opened = ForexOpenedOptions.objects.get(pk=orderid)
                except:
                    return rc.NOT_FOUND
                op = timezone.now()
                time = opened.opendateandtime
                if (op - time) > timedelta(seconds=60) and opened.userid == user:
                    orderid = orderid;
                    currency = opened.currency
                    lots = opened.lots
                    levearage = opened.levearage
                    openprice = opened.openprice
                    bors = opened.bors
                    opendateandtime = opened.opendateandtime
                    if bors == "Short":
                        bs = "Long"
                    else:
                        bs = "Short"
                    closepr = current_price(currency, bs)
                    pl = calc_profit(levearage, lots, openprice, currency, bors)
                    amountnow = pl + (openprice * lots)
                    ranking = ForexRankings.objects.get(userid=user)
                    ranking.balance = ranking.balance + amountnow
                    ranking.equity = ranking.equity + pl
                    ranking.margin = ranking.margin - (openprice * lots * levearage * 10)
                    ranking.save()
                    closed = ForexClosedOptions(userid=user, closedateandtime=timezone.now(), currency=currency, lots=lots, bors=bors, openprice=openprice, levearage=levearage, fltprofit=pl, closeprice=closepr, opendateandtime=opendateandtime)
                    closed.save()
                    opened.delete()
                    if pl > 0:
                        return rc.CREATED
                    else:
                        return rc.CREATED
            else:
                user = request.user.username
                allow = checkparameters(request)
                if allow == 1 or allow == 2:
                    currency = request.POST['currency']
                    bors = request.POST['bors']
                    opened = 0;
                    try:
                        if currency != '0':
                            opened_arr = ForexOpenedOptions.objects.filter(userid=user, currency=currency, bors=bors)
                        else:
                            opened_arr = ForexOpenedOptions.objects.filter(userid=user)
                    except:
                        rc.BAD_REQUEST
        
                    totalpr = 0
                    for opened in opened_arr:
                        op = timezone.now()
                        time = opened.opendateandtime
                        if (op - time) > timedelta(seconds=60):
                            orderid = opened.orderno;
                            currency = opened.currency
                            lots = opened.lots
                            levearage = opened.levearage
                            openprice = opened.openprice
                            bors = opened.bors
                            opendateandtime = opened.opendateandtime
                            if bors == "Short":
                                bs = "Long"
                            else:
                                bs = "Short"
                            closepr = current_price(currency, bs)
                            pl = calc_profit(levearage, lots, openprice, currency, bors)
                            totalpr += pl
                            amountnow = pl + (openprice * lots)
                            ranking = ForexRankings.objects.get(userid=user)
                            ranking.balance = ranking.balance + amountnow
                            ranking.equity = ranking.equity + pl
                            ranking.margin = ranking.margin - (openprice * lots * levearage * 10)
                            ranking.save()
                            closed = ForexClosedOptions(userid=user, closedateandtime=timezone.now(), currency=currency, lots=lots, bors=bors, openprice=openprice, levearage=levearage, fltprofit=pl, closeprice=closepr, opendateandtime=opendateandtime)
                            closed.save()
                            opened.delete()
    
                    if totalpr > 0:
                        rc.CREATED
                    else:
                        rc.CREATED

class ForexPending(BaseHandler):
    allowed_methods = ('GET','DELETE')
    model = ForexPendingOptions  
    
    def read(self, request, orderid=None):
        """
        Returns a single order if `orderid` is given,
        otherwise a subset.
        """
        user = request.user.username
        base = ForexPendingOptions.objects
        page=0
        try:
            page=request.GET['page']
            currency = request.GET['currency']
        except:
            currency = None
            
        start=int(page)*20
        end=int(start)+20
        if orderid:
            return base.get(pk=orderid)
        elif currency:
            return base.filter(userid=user).filter(currency=currency)[start:end]
        else:
            return base.filter(userid=user)[start:end]
    
    @throttle(500, 24*60*60)
    def delete(self, request, pendingid):
        
        allow=checkparameters(request)
        form=CancelOrderForm(request.POST) 
        if form.is_valid() and form.cleaned_data['currency'] in ForexBase.objects.values_list('currency',flat=True):
            if allow==1 or allow==2 :
                try:
                    pending=ForexPendingOptions.objects.get(pendingid=form.cleaned_data['pendingid'])
                    pending.delete()
                    return rc.DELETED
                except:
                    return rc.NOT_FOUND
            else:
                return rc.BAD_REQUEST
        else:
            return rc.BAD_REQUEST
        return rc.BAD_REQUEST
        
class CommodityOpen(BaseHandler):
    allowed_methods = ('GET', 'POST')
    model = CommOpenedOptions   

    @throttle(500, 24*60*60)
    def read(self, request, orderid=None):
        """
        Returns a single order if `orderid` is given,
        otherwise a subset.
        """
        user = request.user.username
        base = CommOpenedOptions.objects
        page=0
        try:
            page=request.GET['page']
            commodity = request.GET['commodity']
        except:
            commodity = None
            
        start=int(page)*20
        end=int(start)+20
        if orderid:
            return base.get(pk=orderid)
        elif commodity:
            return base.filter(userid=user).filter(tradename=commodity)[start:end]
        else:
            return base.filter(userid=user)[start:end]
            
    def create(self, request):
        """
        Creates a new OpenedOption and Saves it. Returns the created Object
        Accepts POST 
        """
        allow=checkparameters(request)
        if allow==1 or allow==2:
            quantity=int(request.POST['lots'])
            pendingprice=request.POST['pendingprice']
            triggerprice = request.POST['triggerprice']
            if triggerprice!="" and not valid_pp(triggerprice):
                return rc.BAD_REQUEST
            if pendingprice!="" and not valid_pp(pendingprice):
                return rc.BAD_REQUEST
            if quantity<1 or quantity >100:
                return rc.BAD_REQUEST
            commodity=request.POST['commodity']
            bors=request.POST['bors']
            levearage = request.POST['levearage']
            lots=int(request.POST['lots'])
            flag=0
            if levearage=="50" or levearage=="100":
                flag = 1
            if not flag==1:
                return rc.BAD_REQUEST
            flag = 0
            if bors=="Long" or bors=="Short":
                flag = 1
            if not flag==1:
                return rc.BAD_REQUEST
            currentprice = find_price(commodity)
            br = currentprice*(.0005)    
            if currentprice==0 or currentprice=="" or currentprice<0:
                return rc.BAD_REQUEST
            output=""
            if not triggerprice=="":
                if bors=="Long":
                    if (float(triggerprice) > currentprice):
                        return rc.CREATED
            if bors == "Short":
                if (float(triggerprice) < currentprice):
                    return rc.CREATED
                if not pendingprice=="":
                    if bors=="Long":
                        if float(pendingprice) > currentprice:
                            return rc.CREATED
                    if bors == "Short":
                        if (float(pendingprice) < currentprice):
                            return rc.CREATED
            
            user=request.user.username;
            if pendingprice=="":
                if triggerprice=="":
                    triggerprice = 0
                else:
                    triggerprice = float(triggerprice)
                try:
                    ranking=ForexRankings.objects.get(userid=user)
                except:
                    return rc.FORBIDDEN
                cost = quantity*(currentprice+br)
                if (currentprice==0 or currentprice=="" or currentprice<0):
                    return rc.BAD_REQUEST
                if (cost==0 or cost=="" or cost<0):
                    return rc.BAD_REQUEST
                brnet = quantity* br
                balance = ranking.balance
                if balance<cost:
                    return rc.CREATED
                balance1 = balance - cost
                c = (-1)*cost
                e = (-1)*brnet
                form_array={'commodity':commodity,'levearage':levearage,'lots':lots,'bors':bors,'currentprice':currentprice,'triggerprice':triggerprice}
                form_array['userid']=request.user.username
                open_form=OpenCommTransaction(form_array)
                if open_form.is_valid():
                    open_form.save()
                else:
                    return rc.BAD_REQUEST
                ranking.margin=ranking.margin+(currentprice*int(levearage)*lots*10) 
                ranking.balance=balance1
                ranking.equity=ranking.equity-brnet
                ranking.save()
                return rc.CREATED
                    
            elif not pendingprice=="":
                if triggerprice=="":
                    triggerprice=0
                else:
                    triggerprice=float(triggerprice)
                if valid_pp(pendingprice):
                    pendingprice=float(pendingprice)
                try:
                    ranking=ForexRankings.objects.get(userid=user)
                except:
                    return rc.BAD_REQUEST
                cost = quantity * triggerprice;
                balance = ranking.balance
                if balance<cost:
                    return rc.CREATED
                form_array={'commodity':commodity,'levearage':levearage,'lots':lots,'bors':bors,'pendingprice':pendingprice,'triggerprice':triggerprice}
                form_array['userid']=user
                pending_form=CommPendingTransaction(form_array)
                if pending_form.is_valid():
                    pending_form.save()
                else:
                    return rc.BAD_REQUEST
                return rc.CREATED
        else:
            return rc.BAD_REQUEST
                        

class CommodityClose(BaseHandler):
    allowed_methods = ('GET', 'POST')
    model = ForexClosedOptions  
    
    def read(self, request, orderid=None):
        """
        Returns a single order if `orderid` is given,
        otherwise a subset.
        """
        user = request.user.username
        base = CommClosedOptions.objects
        page=0
        try:
            page=request.GET['page']
            currency = request.GET['currency']
        except:
            currency = None
            
        start=int(page)*20
        end=int(start)+20
        if orderid:
            return base.get(pk=orderid)
        elif currency:
            return base.filter(userid=user).filter(currency=currency)[start:end]
        else:
            return base.filter(userid=user)[start:end]
        
    @throttle(500, 24*60*60) 
    def create(self, request):
        user=request.user.username
        allow=checkparameters(request)
        if allow==1 or allow==2:
            try:
                orderid=request.GET['orderid'];
            except:
                pass
            opened=0;
            
            try:
                multiple = request.POST['multiple']
            except:
                multiple = None
                
            if not multiple:
                try:
                    opened=CommOpenedOptions.objects.get(orderno=orderid)
                except:
                    return HttpResponse('Position has already been closed.')
                op = timezone.now()
                time = opened.opendateandtime
                if (op-time)>timedelta(seconds=60) and opened.userid==user:
                    commodity=opened.tradename
                    lots=opened.quantity
                    levearage=opened.levearage
                    openprice=opened.openprice
                    bors=opened.bors
                    opendateandtime=opened.opendateandtime
                    if bors=="Short":
                        bs="Long"
                        flag=1
                    else:
                        bs="Short"
                        flag=-1
                    closepr=find_price(commodity)
                    pl=calc_comm_profit(levearage,lots,openprice,commodity,bors)
                    br=brockerage(user,closepr)
                    amount=lots*openprice
                    pl = (closepr - openprice)*lots*levearage*flag 
                    amountnow = amount + pl
                    ranking=ForexRankings.objects.get(userid=user)
                    ranking.balance=ranking.balance+amountnow
                    ranking.equity=ranking.equity+pl
                    ranking.save()
                    closed=CommClosedOptions(userid=user,trandateandtime=timezone.now(),tradename=commodity,quantity=lots,bors=bors,tranprice=openprice,brockerage=br,levearage=levearage,profit=pl,closecp=closepr)
                    closed.save()
                    opened.delete()
                if pl>0:
                    return HttpResponse("<div align='center'><font color='#2A3F00'>Transaction Closed Successfully with profit of $ "+str(pl)+"</font></div>")
                else:
                    return HttpResponse("<div align='center'><font color='red'>Transaction Closed Successfully with loss of $ "+str(-pl)+"</font></div>")
            else:
                user=request.user.username
                allow=checkparameters(request)
                if allow==1 or allow==2:
                    commodity=request.GET['commodity']
                    bors=request.GET['bors']
                    opened=0;
                    try:
                        if commodity!='0':
                            opened_arr=CommOpenedOptions.objects.filter(userid=user,tradename=commodity,bors=bors)
                        else:
                            opened_arr=CommOpenedOptions.objects.filter(userid=user)
                    except:
                        return HttpResponse('Positions have already been closed.')
                    
                    totalpr=0
                    for opened in opened_arr:
                        op = timezone.now()
                        time = opened.opendateandtime
                        if (op-time)>timedelta(seconds=60):
                            orderid=opened.orderno;
                            commodity=opened.tradename
                            lots=opened.quantity
                            levearage=opened.levearage
                            openprice=opened.openprice
                            bors=opened.bors
                            opendateandtime=opened.opendateandtime
                            if bors=="Short":
                                bs="Long"
                                flag=1
                            else:
                                bs="Short"
                                flag=-1
                            closepr=find_price(commodity)
                            pl=calc_comm_profit(levearage,lots,openprice,commodity,bors)
                            br=brockerage(user,closepr)
                            totalpr+=pl
                            amount=lots*openprice
                            pl = (closepr - openprice)*lots*levearage*flag 
                            amountnow = amount + pl
                            ranking=ForexRankings.objects.get(userid=user)
                            ranking.balance=ranking.balance+amountnow
                            ranking.equity=ranking.equity+pl
                            ranking.save()
                            closed=CommClosedOptions(userid=user,trandateandtime=timezone.now(),tradename=commodity,quantity=lots,bors=bors,tranprice=openprice,brockerage=br,levearage=levearage,profit=pl,closecp=closepr)
                            closed.save()
                            opened.delete()
                
                    if totalpr>0:
                        return HttpResponse("<div align='center'><span class='green'>Transactions Closed Successfully with net profit of $ "+str(totalpr)+"</span></div>")
                    else:
                        return HttpResponse("<div align='center'><span class='red'>Transactions Closed Successfully with net loss of $ "+str(-totalpr)+"</span></div>")


class CommodityPending(BaseHandler):
    allowed_methods = ('GET','DELETE')
    model = CommPendingOptions
    
    def read(self, request, orderid=None):
        """
        Returns a single order if `orderid` is given,
        otherwise a subset.
        """
        user = request.user.username
        base = CommPendingOptions.objects
        page=0
        try:
            page=request.GET['page']
            currency = request.GET['currency']
        except:
            currency = None
            
        start=int(page)*20
        end=int(start)+20
        if orderid:
            return base.get(pk=orderid)
        elif currency:
            return base.filter(userid=user).filter(currency=currency)[start:end]
        else:
            return base.filter(userid=user)[start:end]  
    
    def delete(self, request, pendingid):
        allow=checkparameters(request)
        form=CommCancelOrderForm(request.POST)
        if not form.is_valid():
            return rc.BAD_REQUEST
        if form.is_valid() and form.cleaned_data['tradename'] in CommodityBase.objects.values_list('commodity',flat=True):
            if allow==1 or allow==2 :
                pending=CommPendingOptions.objects.get(pendingid=form.cleaned_data['pendingid'])
                pending.delete()
                return rc.DELETED
            else:
                return rc.BAD_REQUEST
        return rc.BAD_REQUEST
    
