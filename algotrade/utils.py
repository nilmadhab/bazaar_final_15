from algotrade.models import Users
from django.contrib.auth.models import User

def authenticate_trade(username,password):
    try:
        user=Users.objects.get(username=username)
        if user.password==password:
            return User.objects.get(username=username)
        else:
            return None
    except:
        return None