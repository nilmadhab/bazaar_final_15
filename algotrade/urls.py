from algotrade.handlers import ForexOpen, CommodityOpen, ForexClose, \
    CommodityClose, CommodityPending, ForexPending, Account, ForexKeys, \
    CommodityKeys, Urls
from algotrade.utils import authenticate_trade
from django.conf.urls import patterns, url
from piston.authentication import HttpBasicAuthentication
from piston.resource import Resource

auth = HttpBasicAuthentication(realm="Kshitij Algorithmic Trading",auth_func=authenticate_trade)

class CsrfExemptResource(Resource):
            """A Custom Resource that is csrf exempt"""
            def __init__(self, handler, authentication=None):
                super(CsrfExemptResource, self).__init__(handler, authentication)
                self.csrf_exempt = getattr(self.handler, 'csrf_exempt', True)
            
forex_open_handler = CsrfExemptResource(ForexOpen,authentication=auth)
comm_open_handler = CsrfExemptResource(CommodityOpen,authentication=auth)
forex_close_handler = CsrfExemptResource(ForexClose,authentication=auth)
comm_close_handler = CsrfExemptResource(CommodityClose,authentication=auth)
comm_pending_handler = CsrfExemptResource(CommodityPending,authentication=auth)
forex_pending_handler = CsrfExemptResource(ForexPending,authentication=auth)
account_handler = CsrfExemptResource(Account,authentication=auth)
forex_keys_handler = CsrfExemptResource(ForexKeys,authentication=auth)
comm_keys_handler = CsrfExemptResource(CommodityKeys,authentication=auth)
urls_handler = CsrfExemptResource(Urls,authentication=auth)

urlpatterns = patterns('',
   url(r'^forex/keys/',forex_keys_handler ,{ 'emitter_format': 'json' }),
   url(r'^urls',urls_handler ,{ 'emitter_format': 'json' }),
   url(r'^commodity/keys/',comm_keys_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/trade/(?P<orderid>[^/]+)/$',forex_open_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/account/',account_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/trade', forex_open_handler,{ 'emitter_format': 'json' }),
   url(r'^commodity/trade/(?P<orderid>[^/]+)/',comm_open_handler ,{ 'emitter_format': 'json' }),
   url(r'^commodity/trade', comm_open_handler,{ 'emitter_format': 'json' }),
   url(r'^commodity/pending/(?P<pendingid>[^/]+)/',comm_pending_handler ,{ 'emitter_format': 'json' }),
   url(r'^commodity/pending',comm_pending_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/pending/(?P<pendingid>[^/]+)/',forex_pending_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/pending',forex_pending_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/close/(?P<orderid>[^/]+)/',forex_close_handler ,{ 'emitter_format': 'json' }),
   url(r'^forex/close', forex_close_handler,{ 'emitter_format': 'json' }),
   url(r'^commodity/close/(?P<orderid>[^/]+)/',comm_close_handler ,{ 'emitter_format': 'json' }),
   url(r'^commodity/close', comm_close_handler,{ 'emitter_format': 'json' }),
)



