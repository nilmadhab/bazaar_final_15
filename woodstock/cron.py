from django_cron import cronScheduler, Job
from cron_functions import *

class UpdateOpeningPrice(Job):
	run_every = 60*60*24
	
	def job(self):
		updateopeningprice()

cronScheduler.register(UpdateOpeningPrice)

class UpdateRanks(Job):
	run_every = 1800
	
	def job(self):
		updateranks()
                

cronScheduler.register(UpdateRanks)

class UpdateLastFriday(Job):
	run_every = 60*60*24
	
	def job(self):
		updatelastfriday()
                

cronScheduler.register(UpdateLastFriday)

class UpdateLastMonday(Job):
	run_every = 60*60*24
	
	def job(self):
		updatelastmonday()    
    	
cronScheduler.register(UpdateLastMonday)

class StrigClose(Job):
	run_every = 600
	
	def job(self):
		strigclose()
                
cronScheduler.register(StrigClose)

class UpdateOpenOrders(Job):
	run_every = 300
	def job(self):
		updateopenorders()
                
cronScheduler.register(UpdateOpenOrders)

class UpdateCloseOrders(Job):
	run_every = 60*60*24
	
	def job(self):
		updatecloseorders()
       
cronScheduler.register(UpdateCloseOrders)


class PriceUpdate(Job):
	run_every = 60*15
	
	def job(self):
		priceupdate()
               
cronScheduler.register(PriceUpdate)

class Diff(Job):
	run_every = 60*60*24
	
	def job(self):
		diff()

cronScheduler.register(Diff)

class CancelPendingOrder(Job):
	run_every = 300
	
	def job(self):
		cancelpendingorder()

cronScheduler.register(CancelPendingOrder)

class ftrigClose(Job):
	run_every = 300
	
	def job(self):
		ftrigclose()

cronScheduler.register(ftrigClose)

class WdOpen(Job):
	run_every = 60*60*24
	
	def job(self):
		wdopen()

cronScheduler.register(WdOpen)

class WriteStock(Job):
	run_every = 300
	
	def job(self):
		writestock()

cronScheduler.register(WriteStock)

class FridayOptions(Job):
	run_every = 60*60*24
	
	def job(self):
		fridayoptions()

cronScheduler.register(FridayOptions)

class WeeklyVolatility(Job):
	run_every = 60*60*24
	
	def job(self):
		weeklyvolatility()

cronScheduler.register(WeeklyVolatility)

class uPrice(Job):
	run_every = 300
	
	def job(self):
		uprice()

cronScheduler.register(uPrice)

class UpdateM(Job):
	run_every = 60*60*24
	
	def job(self):
		updateM()

cronScheduler.register(UpdateM)

class CurrentPremium(Job):
	run_every = 300
	
	def job(self):
		currentpremium()

cronScheduler.register(CurrentPremium)

class FutureDiff(Job):
	run_every = 300
	
	def job(self):
		futureDiff()

cronScheduler.register(FutureDiff)

class ContractFutures(Job):
	run_every = 300
	
	def job(self):
		contractFutures()

cronScheduler.register(ContractFutures)

class DailyFutures(Job):
	run_every = 60*60*24
	
	def job(self):
		dailyFutures()

cronScheduler.register(DailyFutures)

class WriteFutures(Job):
	run_every = 300
	
	def job(self):
		writeFutures()

cronScheduler.register(WriteFutures)

class StartUpdateAll(Job):
	run_every = 60*60*24
	
	def job(self):
		startUpdateAll()

cronScheduler.register(StartUpdateAll)

class FridayFutures(Job):
	run_every = 60*60*24
	
	def job(self):
		fridayFutures()

cronScheduler.register(FridayFutures)

class StartUpdate(Job):
	run_every = 60*60*24
	
	def job(self):
		startUpdate()

cronScheduler.register(StartUpdate)




