import datetime, MySQLdb
from data.models import *
from custom.func import *
from math import exp, log
from random import randint
from datetime import time
import re, string, urllib2

def updateopeningprice():
	try:
		print('entered updateopeningprice')
		no = 1
		que = StocksRate.objects.all()
		if(que.count == 0):
			print('No stocks exist')
			return
		for q in que:
			print('entered loop')
			openingprice = q.price
			stock = q.stock
			series = q.series

			q.open = openingprice
			q.save()
			print('saved')

			no=no+1

			print('Updated the opening price of '+stock+' at price '+str(openingprice)+' series')
	except:
		return

def updateranks():
	try:
		print('entered updateranks yeah')
		que = WudRankings.objects.all().order_by('-equity')
		print('fetched')
		rank = 0

		for q in que:
			rank = rank+1
			q.rank = rank
			q.save()
			print(str(rank)+'. '+q.userid)

		print('updateranks completed')
	except:
		return

def updatelastfriday():
	try:
		print('entered updatelastfriday')
		#q = Status.objects.all()

		today = datetime.date.today().weekday()
		if (today == 5):
			no = 1
			q2 = StocksRate.objects.all()
			if (q2.count == 0):
				print('No stocks exist')
				return
			for r2 in q2:
				fridayprice = r2.price
				stock = r2.stock
				series = r2.series

				r2.lastfriday = fridayprice
				r2.save()

				no = no+1
				print(str(no)+'. updated the lastfriday price of '+stock+' at price '+str(fridayprice)+' of series ')
		else:
			print('today is not desired friday!!!')
	except:
		return

def updatelastmonday():
	try:
		print('entered updatelastmonday')
		#q = Status.objects.all()

		today = datetime.date.today().weekday()
		if(today != 1):
			print('not monday')
			return
		if (arr.status == 1):
			no = 1
			q2 = StocksRate.objects.all()
			if (q2.count == 0):
				print('No stocks exist')
				return
			for r2 in q2:
				mondayprice = r2.price
				stock = r2.stock
				series = r2.series

				r2.lastmonday = mondayprice
				r2.save()

				no = no+1
				print(str(no)+'. '+'update the lastmonday price of '+stock+' at price '+str(mondayprice)+' of series '+series)
			else:
				print('Today is not desired monday')
	except:
		return

def strigclose():
	try:
		print('entered strigclose')
		marginfrac = 0.5
		q= WudOpenedOptions.objects.all()
		if (q.count == 0):
			print('No users exist')
			return
		for arr in q:
			userid = arr.userid
			stockname = arr.stockname
			orderno = arr.orderno
			oldquantity = arr.quantity
			#pendingprice = request.GET['pendingprice']
			cp = arr.tranprice
			bors = arr.bors
			sellprice = current_price(stockname)
			br = brockerage_fun(arr.userid, sellprice)
			brockerage = oldquantity*sellprice
			triggerprice = arr.triggerprice
			trigcheck = 0

			if (triggerprice != 0):
				if (arr.bors == 'Buy'):
					if (sellprice < triggerprice):
						trigcheck = 1
						amount = oldquantity*triggerprice
						loss = (cp - triggerprice)*oldquantity
				else: 
					if (sellprice > triggerprice):
						trigcheck = 1
						amount = oldquantity*triggerprice
						loss = (triggerprice-cp)*oldquantity

			if (trigcheck == 1):
				if (bors == 'Buy'):
					q2 = WudRankings.objects.get(userid=userid)
					q2.reserve = q2.reserve+amount-brockerage
					q2.equity = q2.equity-brockerage-loss
					q2.save()

					p = amount - brockerage
					zero = (-1)*(brockerage-loss)

					qded = WudUserDeductions(userid=userid,orderno=orderno,bal_ded=p,worth_ded=zero)
					qded.save()

					loss1 = (-1)*loss

					q3 = WudClosedOptions(userid=userid,trandateandtime=datetime.datetime.now(),stockname=stockname,bors='Sell',quantity=oldquantity,tranprice=triggerprice,brockerage=brockerage,profit=loss1,closecp=cp)
					q3.save()

					q1 = WudOpenedOptions.objects.get(bors='Buy',stockname=stockname,userid=userid)
					q1.delete()

					print('Deleted '+userid+'`s order of '+stockname)

				elif (bors != 'Buy'):
					add_margin = arr.margin

					update = WudRankings.objects.get(userid=userid)
					update.margin = update.margin-add_margin
					update.save()

					print('profit')

					porl = 0
					porl = add_margin*round((1/marginfrac),0)-amount
					porl = round(porl,2)

					qq = add_margin+porl

					q2 = WudRankings.objects.get(userid=userid)
					q2.reserve = q2.reserve+qq-brockerage
					q2.equity = q2.equity-brockerage+porl
					q2.save()

					p = qq-brockerage
					zero = porl-brockerage
					qded = WudUserDeductions(userid=request.user,orderno=orderno,bal_ded=p,worth_ded=zero)
					qded.save()

					q3 = WudClosedOptions(userid=userid,trandateandtime=datetime.datetime.now(),stockname=stockname,bors='Short Cover',quantity=oldquantity,tranprice=cp,brockerage=brockerage,closecp=sellprice,profit=porl)
					q3.save()

					q4 = WudOpenedOptions.objects.get(bors='Short Sell',stockname=stockname,userid=userid)
					q4.delete()

					print('deleted '+str(userid)+'`s order of '+stockname)
	except:
		return

def updateopenorders():
	try:
		print('entered updateopenorders')
		marginfrac = 0.5

		que = WudPendingOptions.objects.all()

		for q in que:
			print('working')

		for q in que:
			print('entered loop')
			quantity = q.quantity
			o = q.pendingid
			currentprice = current_price(q.stockname)
			br = brockerage_fun(q.userid,currentprice)
			brockerage = br*int(quantity)

			pp = q.pendingprice
			req = 0
			quan = 0
			stockname = q.stockname
			bors = q.bors
			flag = 0
			print('saikiran')
			if(bors == 'Buy'):
				if(currentprice<=q.pendingprice):
					flag = 1
			else:
				if(currentprice>=q.pendingprice):
					flag = 1

			if(flag == 1):
				if(bors == 'Buy'):
					print('came here too')
					query = WudRankings.objects.get(userid=q.userid)
					print(q.userid+' : ')
					req = quantity*q.pendingprice+brockerage
					if(query.reserve<req):
						quan = int(query.reserve)/currentprice
						print('you can only buy '+str(quan)+' stocks')

						q1 = WudPendingOptions.objects.get(userid=q.userid,pendingid=q.pendingid)
						q1.delete()
						print('deleted')
					else:
						query.reserve = query.reserve-req
						query.equity = query.equity-brockerage
						query.save()
						print('saved')
						p = (-1)*req
						zero = (-1)*brockerage

						qded = WudUserDeductions(userid=q.userid,orderno=o,bal_ded=p,worth_ded=zero)
						qded.save()
						print('deduction saved')

						opened = WudOpenedOptions.objects.filter(bors='Buy',stockname=stockname,userid=q.userid)
						if(opened.count != 0):
							for opt in opened:
								opt.quantity=opt.quantity+quantity
								opt.save()
								print('updating opened options')
						else:
							que2 = WudOpenedOptions(userid=q.userid,trandateandtime=datetime.datetime.now(),stockname=stockname,bors=bors,quantity=quantity,tranprice=pp)
							que2.save()
							print('inserted opened option')
						q3 = WudClosedOptions(userid=q.userid,trandateandtime=datetime.datetime.now(),stockname=stockname,bors=bors,quantity=quantity,tranprice=pp,brockerage=brockerage)
						q3.save()

						print('inserted closed option')
						print('You have successfully bought '+str(quantity)+' stock holdings')

						qdel = WudPendingOptions.objects.get(pendingid=q.pendingid)
						qdel.delete()
				else:
					rank = WudRankings.objects.get(userid=q.userid)
					print(q.userid+" : ")

					req=quantity*pp
					if(rank.reserve<req):
						quan = int(rank.reserve/pp)
						print('you can only short sell '+str(quan)+' stocks')
					else:
						rank.reserve = rank.reserve-((req*marginfrac)+brockerage)
						rank.equity = rank.equity - brockerage
						rank.margin = rank.margin + (req*marginfrac)
						rank.save()

						p = (-1)*((req*marginfrac)+brockerage)
						zero = (-1)*brockerage

						qded2 = WudUserDeductions(userid=q.userid,orderno=o,bal_ded=p,worth_ded=zero)
						qded2.save()

						que3 = WudClosedOptions(userid=q.userid,trandateandtime=datetime.datetime.now(),stockname=stockname,bors=bors,quantity=quantity,tranprice=currentprice,brockerage=brockerage)
						que3.save()

						print('Successfully short sold '+str(quantity)+' stock holdings')

						qdel2 = WudPendingOptions.objects.get(pendingid=q.pendingid)
						qdel2.delete()

		print('open orders completed')
	except:
		return


def updatecloseorders():
	try:
		print('entered updatecloseorders')
		marginfrac=0.5
		pq = WudPendingCloseOptions.objects.all()

		for pa in pq:
			stockname=pa.stockname
			opencp = pa.tranprice
			margin = pa.margin
			quantity = pa.quantity
			pendingprice = pa.pendingprice
			userid = pa.userid
			bors = pa.bors
			orderno = pa.orderno
			currentprice = current_price(stockname)
			br = brockerage_fun(pa.userid,currentprice)
			brockerage = br*quantity

			flag = 0
			fact = 0
			sellprice = currentprice
			if(bors == 'Buy'):
				if(currentprice >= pendingprice):
					flag=1
					fact=1
			else:
				if(currentprice <= pendingprice):
					flag = 1
					fact = -1

			profit = (pendingprice-opencp)*quantity
			amount = (pendingprice)*quantity

			if(flag == 1):
				if(bors=='Buy'):
					print(pa.userid+" : ")
					q2 = WudRankings.objects.get(userid=userid)
					q2.reserve = q2.reserve+amount-brockerage
					q2.equity = q2.equity+profit-brockerage
					q2.save()
					print('saved')
					p = amount-brockerage
					zero = profit-brockerage
					qded = WudUserDeductions(userid=userid,orderno=orderno,bal_ded=p,worth_ded=zero)
					qded.save()
					print('deduction saved')
					q3 = WudClosedOptions(userid=userid,orderno=orderno,trandateandtime=datetime.datetime.now(),stockname=stockname,bors='Sell',quantity=quantity,tranprice=sellprice,brockerage=brockerage, closecp=sellprice, profit=profit)
					q3.save()
					print('closed option saved')
					qdel = WudPendingCloseOptions.objects.get(orderno=pa.orderno)
					qdel.delete()
					print('Successfully sold '+str(quantity)+' stock holdings')
				else:
					print(pa.userid+' : ')
					add_margin = marginfrac
					
					porl = 0
					porl = add_margin*round((1/marginfrac),0)-amount-brockerage
					porl = round(porl,2)

					q4 = WudRankings.objects.get(userid=userid)
					q4.reserve = q4.reserve+(add_margin+porl)
					q4.equity = q4.equity+porl
					q4.margin = q4.margin-margin
					q4.save()
					print('sell ranking saved')
					p = add_margin+porl
					zero = porl
					qded = WudUserDeductions(userid=userid,orderno=orderno,bal_ded=p,worth_ded=zero)
					qded.save()
					print('sell ded saved')
					q5 = WudClosedOptions(userid=userid,trandateandtime=datetime.datetime.now(),stockname=stockname,bors='Short Cover',quantity=quantity,tranprice=sellprice, brockerage=brockerage, closecp=sellprice,profit=porl)
					q5.save()
					print('sell closed saved')
					qdel = WudPendingCloseOptions.objects.get(orderno=pa.orderno)
					qdel.delete()
					print('Successfully covered short '+str(quantity)+' stock holdings')
	except:
		return


def priceupdate():
	print('entered priceupdate')
	try:
		arr = StocksRate.objects.all()
		i=0
		index = 0
		for ar in arr:
			index = index+1
			amp = False
			#f = ar.strip()
			f = ar.compstatname.strip()
			check = 0
			data = ''
			for c in f:
				if(c == '&'):
					amp = True
					stocksplit = f.split('&')
			if(amp):
				stocksplit[0] = stocksplit[0].strip()
				stocksplit[1] = stocksplit[1].strip()
				stocksplit[1] = stocksplit[1]+'.NS'
				l = 'http://finance.yahoo.com/q?s='+stocksplit[0]+'%26'+stocksplit[1]
			else:
				f = f+'.NS'
				l = 'http://finance.yahoo.com/q?s='+f
			
			stock = l.split('=')[1].lower()
			try:
				res = urllib2.urlopen(l)
				result = res.read()
			except:
				ar.delete()
				print('deleted')
				continue
			search = 'yfs_l84_'+stock.strip()
			search = search.decode('utf-8')
			print(search)
			i = result.decode('utf-8').find(search)
			j = 8+len(stock)+2
			tar = result[i: i+40]
			print(tar)
			k = tar.find('<')
			
			if(k != -1):
				price = tar[j:k]
				price = re.sub(r'[^\d.]+', '', price)
				print('refined '+price)
				stock=stock[:-3].upper()
				try:
					#a = StocksRate.objects.get(compstatname=stock)
					lastPrice = ar.price
					mon = ar.lastmonday
					diffmin = float(price)-float(lastPrice)
				except:
					continue
				
				print(price+' after diffmin')
				if(price != 0):
					ar.price = price
					ar.lastmin = lastPrice
					ar.diffmin = diffmin
					ar.save()
					print('current price is '+str(price)+' at diffmin = '+str(diffmin)+' with mon price = '+str(mon)+' and Lastprice = '+str(lastPrice))
			else:
				ar.delete()
				print('deleted')
				
	except:
		return


def diff():
	try:
		print('entered diff')
		#q = Status.objects.all()

		today = datetime.date.today().weekday()

		if(today == 1):
			no = 1
			q2 = StocksRate.objects.all()
			if(q2.count == 0):
				print('No stocks exist')
				return
			for arr2 in q2:
				mondayprice = arr2.price
				stock = arr2.stock
				series = arr2.series
				diff = float(arr2.lastfriday) - float(arr2.lastmonday)

				arr2.diff = diff
				arr2.save()

				no=no+1
				print('Updated the diff of '+stock+' at price '+str(diff)+' of series '+series)
		else:
			print('Today is not desired friday')
	except:
		return


def cancelpendingorder():
	try:
		print('entered cancelpendingorder')

		'''que = WudPendingCloseOptions.objects.all()

		for arr in que:
			quantity = arr.quantity
			stockname=arr.stockname
			bors = arr.bors
			user=arr.userid
			q2 = WudOpenedOptions.objects.filter(userid=user,stockname=stockname,bors=bors)

			if(q2.count != 0):
				for r2 in q2:
					r2.quantity = r2.quantity+quantity
					r2.save()
			else:
				q3 = WudOpenedOptions(userid=request.user,trandateandtime=datetime.datetime.now(),stockname=stockname,bors=arr.bors,quantity=quantity,tranprice=currentprice)
				q3.save()

			qdel = WudPendingCloseOptions.objects.get(orderno=orderno,stockname=stockname,bors=bors,userid=request.user)
			qdel.delete()'''
	except:
		return

def ftrigclose():
	try:
		print('entered ftrigclose')
		counter = 1
		query = FuturesOpenedOrClosed.objects.all()

		if(query.count == 0):
			print('no transactions were found')
			return
		else:
			for arr in query:
				print('entered loop')
				stock = arr.stock
				quantity = arr.quantity
				margin = arr.margin
				opencp = arr.opencp
				profitcp = arr.profitcp
				lors = arr.lors
				expiry = arr.expiry
				expiryno = arr.expiryno
				time = arr.time
				userid = arr.userid
				triggerprice = arr.triggerprice
				trigcheck = 0
				orderid = arr.orderid
				status = arr.status
				print('all values OK')

				if(status == 0):
					continue
				if(triggerprice!=0):
					q3 = FuturesBase.objects.get(javaname=stock)

					closecp = q3.contractprice2
					if(lors == 'Long'):
						if(closecp < triggerprice):
							flag = 1
							trigcheck = 1
					if(lors == 'Short'):
						if(closecp > triggerprice):
							flag = -1
							trigcheck = 1

					if(trigcheck == 1):
						print('trig checked')
						profit = (triggerprice - profitcp)*flag*quantity
						ret = quantity*margin*opencp

						arr.profitcp = closecp
						arr.profit = profit
						arr.status = 0
						arr.save()
						print(str(counter)+'. Removed '+arr.userid+'`s order of '+stock)

						counter=counter+1
						q10 = WudRankings.objects.get(userid=userid)
						reserve = q10.reserve
						margina = q10.margin
						equity = q10.equity

						reserve = reserve+profit+ret
						equity = equity+profit
						margina = margina-ret

						q10.reserve = reserve
						q10.equity = equity
						q10.margin = margina
						q10.save()
						print('updated ranking')
						p = profit+ret
						qded = WudUserDeductions(userid=userid,orderno=orderid,bal_ded=p,worth_ded=profit)
						qded.save()
						print('deduction saved')

			if(counter == 1):
				print('no entries were removed')
			else:
				counter = counter-1
			print(str(counter)+' entries were removed')
	except:
		return

def wdopen():
	try:
		print('entered wdopen')

		if(value == 1):
			q = MarketOpen.objects.get(oc=0)
			q.oc = 1
			q.save()
			print('Done. value = '+str(value)+'. Market is now open')
		if(value == 0):
			q = MarketOpen.objects.get(oc=2)
			q.oc = 0
			q.save()
			print('Done. value = '+str(value)+'. Market is now closed completely')
		if(value == 2):
			q = MarketOpen.objects.get(oc=1)
			q.oc = 2
			q.save()
			print('Done. value = '+str(value)+'. Market is now open only for Forex')
	except:
		return

def writestock():
	try:
		print('entered writestock')
		q1 = StocksRate.objects.all()
		with open('woodstock/static/files/admin/stock.txt','w+') as f:
			for q in q1:
				string = q.stock+'~'+str(q.price)+'~'
				f.write(string)
			content = f.read()
			f.close()
		print(content)
	except:
		return

import datetime

def fridayoptions():
	try:
		print('entered fridayoptions')
		today = datetime.date.today().weekday()
		if(today != 5):
			print('options cannot be closed')
			return

		today = datetime.date.today()
		status = 1

		q1 = OptionsOpenedOrClosed.objects.filter(expiry=today,status=status)
		for arr in q1:
			print('entered loop')
			stock = arr.name
			lors = arr.longorshort
			orderno = arr.transactionid
			corp = arr.callorput
			mar = arr.margin
			quantity = arr.quantity
			user = arr.userid
			sp = arr.strikeprice
			transactionid = arr.transactionid
			stockfull = arr.name
			print('values OK')
			
			st = stockfull.split('.')
			stockname_java = st[0]
			try:
				qjava = StocksRate.objects.get(stock=stockname_java)
				up = qjava.price
			except:
				continue

			q2 = WudRankings.objects.get(userid=user)
			cashbalance = q2.cashbalance
			reserve = q2.reserve
			equity = q2.equity
			margin = q2.margin
			payoff = (up-sp)*quantity
			if(lors == 'Long'):
				if(corp == 'Call'):
					if(payoff > 0):
						cashbalance1 = cashbalance+payoff
						reserve1 = reserve+payoff
						equity1 = equity+payoff

						q3 = OptionsOpenedOrClosed.objects.get(transactionid=transactionid)
						q3.profit = payoff
						q3.save()

						q2.equity=equity1
						q2.reserve = reserve1
						q2.save()
						print('long call transaction of '+user+' for '+stock+' closed successfully with profit of Rs.'+str(payoff))
					else:
						print('long call transaction of '+user+' for '+stock+' closed successfully')

				if(corp == 'Put'):
					if(payoff<0):
						payoff2 = (-1)*payoff
						cashbalance2 = cashbalance+payoff2
						reserve2 = reserve+payoff2
						equity2 = equity+payoff2

						q3 = OptionsOpenedOrClosed.objects.get(transactionid=transactionid)
						q3.profit = payoff
						q3.save()

						q2.equity = equity2
						q2.reserve = reserve2
						q2.save()

						print('Long call transaction of '+user+' for '+stock+' closed successfully for the profit of Rs.'+str(payoff2))
					else:
						print('Long put transaction of '+user+' for '+stock+' closed successfully')
				
			if(lors == 'Short'):
				if(corp == 'Put'):
					if(payoff < 0):
						cashbalance1 = cashbalance+payoff
						reserve1 = reserve+payoff+mar
						equity1=equity+payoff
						margin1 = margin - mar

						q3 = OptionsOpenedOrClosed.objects.get(transactionid=transactionid)
						q3.profit = payoff
						q3.save()

						q2.equity = equity1
						q2.reserve = reserve1
						q2.margin = margin1
						q2.save()

						print('Short Put transaction of '+user+' for '+stock+' closed successfully with loss of Rs.'+str(payoff))
					else:
						margin1 = margin-mar
						q2.margin = margin1
						q2.save()

						print('Short put transaction of '+user+' for '+stock+' closed successfully')

				if(corp == 'Call'):
					if(payoff>0):
						payoff2 = (-1)*payoff
						cashbalance2 = cashbalance+payoff2
						reserve2 = reserve+payoff2
						equity2=equity+payoff2
						margin2 = margin-mar

						q3 = OptionsOpenedOrClosed.objects.get(transactionid=transactionid)
						q3.profit = payoff
						q3.save()

						q2.equity=equity2
						q2.reserve=reserve2
						q2.margin=margin2
						q2.save()

						print('Short Call transaction of '+user+' for '+stock+' closed successfully for the profit of Rs.'+str(payoff))
					else:
						margin2 = margin - mar
						
						q2.margin = margin2
						q2.save()

						print('Short call transaction of '+user+' for '+stock+' closed successfully')

			q7 = OptionsOpenedOrClosed.objects.get(transactionid=transactionid)
			q7.status = 0
			q7.save()
	except:
		return


def weeklyvolatility():
	try:
		print('entered weeklyvolatility')
		q = Status.objects.all()

		for arr in q:
			print('entered')
			if(arr.status == 1):
				volatility_main()
			else:
				print('today is not desired friday')
	except:
		return

def uprice():
	try:
		count = 1
		que = OptionsBase.objects.all()
		for arr in que:
			stocksplit = arr.stock.split('.')
			stocknow = stocksplit[0]
			try:
				q = StocksRate.objects.get(stock=stocknow)
				price = q.price
			except:
				continue
			arr.uprice = price 
			arr.save()

			print(str(count)+'. Updated'+arr.stock+' at price '+str(price))

			count=count+1
	except:
		return

def updateM():
	try:
		print('entered updatem')
		#from random import randint 

		que = Status.objects.all()
		randArray = []
		
		for arr in que:
			if(arr.status == 1):
				q1 = OptionsBase.objects.all()
				for a1 in q1:
					name = a1.stock
					i=0
					for x in xrange(0,11):
						try:
							randArray[x] = round(5+(20*randint(0, 32767)/32767),2)
						except IndexError: 
							randArray.append(round(5+(20*randint(0, 32767)/32767),2))

					randArray.sort()
					randM = '~'.join(str(x) for x in randArray)

					a1.randm = randM
					a1.save()

					print('randM of '+name+' updated to '+str(randM))
			else:
				print('Today is not friday')
	except:
		return


def currentpremium():
	try:
		print('entered currentpremium')
		que = OptionsBase.objects.all()

		for arr in que:
			stock = arr.javaname
			stockpre = stock.split('.')

			variable = stockpre[0]
			try:
				qnow = StocksRate.objects.get(stock=variable)
				up = float(qnow.price)
			except:
				continue
			print(stock+' '+str(up))
			nowpr = stock.split('.')
			name = nowpr[0]
			
			strikeprice = arr.strikeprice.split('~')
			call = arr.currentcall2.split('~')
			callbweek = arr.callbpbiweek
			price_checker = arr.uprice
			callBweekChunk = callbweek.split('~')
			old = arr.currentcall2
			oldChunk = old.split('~')
			
			premium =[]
			val = []
			
			for x in xrange(0,10):
				try:
					premium[x] = float(oldChunk[x]) + float(dailyPremium(stock,x,callBweekChunk[x],'Call')) ##################func.php##############
				except IndexError:
					print(x)
					print(callBweekChunk[x])
					print(dailyPremium(stock,x,callBweekChunk[x],'Call'))
					try:
						premium.append(float(oldChunk[x]) + float(dailyPremium(stock,x,callBweekChunk[x],'Call')))
					except:
						return
				if(price_checker < 100):
					if(premium[x]< 6):
						premium[x] = 5+float(randint(300000,320000)/320000)
					premium[x] = round(premium[x],3)
				elif((price_checker > 100) and (price_checker< 500)):
					if(premium[x]< 10):
						premium[x] = 9+float(randint(300000,320000)/320000)
					premium[x] = round(premium[x],3)
				elif(price_checker>500):
					if(premium[x]<15):
						premium[x] = 14+float(randint(300000,320000)/320000)
					premium[x] = round(premium[x],3)

				#try:
				print('entered try block')
				if(premium[x]>premium[x-1]):
					premium[x] = premium[x-1] - float(randint(300000,320000)/320000)
					if(premium[x]<0):
						premium[x-1] = premium[x-2]-(.5)*(float(randint(300000,320000)/320000))
						premium[x] = premium[x-1] - float(randint(300000,320000)/320000)
					premium[x-1] = round(premium[x-1],3)

					if(premium[x]<0):
						premium[x] = premium[x-1] - 0.01*float(randint(300000,320000)/320000)
					if(premium[x]<0):
						premium[x] = premium[x-1] - .001*float(randint(300000,320000)/320000)

					premium[x] = round(premium[x],3)
					print('exited try block')
				#except:
				#	pass
				strikeprice[x] = float(strikeprice[x])
				#premium[x] = float(premium[x])
				try:
					val[x] = (-1)*(up - strikeprice[x] - premium[x])
				except IndexError:
					val.append((-1)*(up - strikeprice[x] - premium[x]))
				if(strikeprice[x] < up):
					if( (strikeprice[x] + premium[x] - up) > (05*up)):
						premium[x] = 1.05*up - strikeprice[x]
						print('Cap has been used')

				if((up - strikeprice[x]) > premium[x]):
					if((up-strikeprice[x]) > premium[x]):
						premium[x] = 1.06*(up-strikeprice[x]) + (0.01*up)

			valcall = '~'.join(str(x) for x in val)
			premium2 = '~'.join(str(x) for x in premium)

			putBweek = arr.putbpbiweek
			putBweekChunk = putBweek.split('~')
			old = arr.currentput2
			oldChunk = old.split('~')
			
			val1 = []
			for x in xrange(0,11):
				try:
					premium[x] = float(oldChunk[x]) + float(dailyPremium(stock,x,putBweekChunk[x],'Put'))
				except IndexError:
					premium.append(float(oldChunk[x]) + float(dailyPremium(stock,x,putBweekChunk[x],'Put')));
				if(premium[x]<0.05):
					premium[x] = 0.05
				if(price_checker<100): 
					if(premium[x]<6):
						premium[x] = 5+float(randint(300000,320000)/320000)
					premium[x] = round(premium[x],3)
				elif((price_checker>100) and (price_checker<500)):
					if(premium[x]<10):
						premium[x]=9+float(randint(300000,320000)/320000)
					premium[x] = round(premium[x],3)
				elif(price_checker>500):
					if(premium[x] < 15):
						premium[x]=14+float(randint(300000,320000)/320000)
					premium[x]=round(premium[x],3)

				try:
					if(premium[x]<premium[x-1]):
						premium[x] = premium[x-1] + float(randint(300000-320000)/320000)
						premium[x] = round(premium[x],3)
				except:
					pass

				strikeprice[x] = float(strikeprice[x])
				premium[x] = float(premium[x])

				try:
					val1[x] = up - float(strikeprice[x]) + float(premium[x])
				except IndexError:
					val1.append(up - float(strikeprice[x]) + float(premium[x]))
				var = strikeprice[x] - up
				if(strikeprice[x] > up):
					if( (up-strikeprice[x]+premium[x]) > (0.05*up)):
						premium[x] = strikeprice[x] - .95*up
						print('Cap has been used')
				if((strikeprice[x] - up) > premium[x]):
					print('error detected')
					premium[x] = oldChunk[x]
					if((strikeprice[x] - up)> premium[x]):
						var100 = 0.01*up
						print(var100)
						premium[x] = 1.06*(strikeprice[x]-up)+var100

			valcall1 = '~'.join(str(x) for x in val1)
			premium5 = '~'.join(str(x) for x in premium)

			arr.currentcall2 = premium2
			arr.checkcall = valcall
			arr.checkput = valcall1
			arr.currentput2 = premium5
			arr.save()
	except:
		return


def futureDiff():
	try:
		print('entered futurediff')
		no = 1
		que2 = FuturesBase.objects.all()
		if(que2.count() == 0):
			print('No stocks exist')
			return

		for arr in que2:
			stock = arr.stock
			contract2 = arr.contractprice2
			stockname = stock.split('.')
			sto = stockname[0]
			try:
				arr1 = StocksRate.objects.get(stock=sto)
			except:
				continue
			
			count = 0
			if count == 0:
				price = arr1.price

				diff2 = float(price)-float(contract2)
				print(sto+'<br>'+'ContractPrice2 '+str(contract2)+'<br> Price '+str(price)+'<br> diff2'+str(diff2)+'<br>')

				arr.diff2 = diff2
				arr.save()

				no=no+1
				print(str(no)+'. Updated the diff of '+stock+' at prices '+str(diff2))

			count = count+1
			print('<br>')
	except:
		return


def contractFutures():
	try:
		que = FuturesBase.objects.all()
		for q in que:
			cp2 = roundOff(contractprice(q.javaname,2))

			q.contractprice2=cp2
			q.save()

			print(q.stock+' contract prices updated respectively by '+str(cp2))
	except:
		return


def dailyFutures():
	import time
	try:
		que = WudRankings.objects.all()
		today = time.strftime('%m %d %Y',time.localtime())

		for arr in que:
			balance = arr.cashbalance
			reserve = arr.reserve
			equity = arr.equity
			Id = arr.pk
			status = 1

			que2 = FuturesOpenedOrClosed.objects.all()

			for arr2 in que2:
				stock = arr2.stock
				quantity = arr2.quantity
				margin = arr2.margin
				expiryno = arr2.expiryno
				lors = arr2.lors
				profitcp = arr2.profitcp
				orderid = arr2.orderid
				expirydate = arr2.expiry
				orderno = arr2.orderid
				if(expirydate == today):
					print(str(Id)+' not updated by this script')
					continue
				try:
					que3 = FuturesBase.objects.get(javaname=stock)
				except:
					continue

				closecp = que3.contractprice2
				if(lors == 'Long'):
					flag=1
				if(lors=='Short'):
					flag=-1

				profit = (float(closecp)-float(profitcp))*flag*quantity
				print(str(Id)+' earned a profit of Rs. '+str(profit))

				que2.profitcp =  profit
				que2.save()

				balance = balance+profit
				reserve = reserve+profit
				equity = equity+profit

				print(str(Id)+' has Rs. '+str(balance)+' in account ')

		que1 = FuturesBase.objects.all()
		for arr1 in que1:
			java = arr1.javaname
			javachunk = java.split('.')
			javaname = javachunk[0]

			try:
				que2 = StocksRate.objects.get(stock=javaname)
			except:
				continue
			price = que2.price

			arr1.closeprice = price
			arr1.save()

			print('Close price of '+javaname+' updated to '+str(price))
	except:
		return

def writeFutures():
	try:
		print('entered writefutures')
		que = FuturesBase.objects.all().order_by('stockid')
		with open('woodstock/static/files/futures/fstock.txt','w+') as f:
			for q in que:
				name = q.javaname
				nameChunk = name.split('.')
				nameNew = nameChunk[0]

				que2 = StocksRate.objects.get(stock=nameNew)
				stockName = que2.stock
				price = que2.price
				stringData = que2.compstatname+'~'+str(que2.price)+'~'
				print(stringData)
				
				f.write(stringData)

			#content = f.read()
			f.close()

			#print(content)
	except:
		return

def startUpdateAll():
	from datetime import datetime
	try:
		print('entered startUpdateAll')
		que = Status.objects.all()
		today = datetime.date.today().weekday()

		if(today != 5):
			print('Today is not friday')
			return

		for arr in que:
			if(arr.status == 1):
				today = datetime.today()
				que1 = FuturesBase.objects.all()

				for arr2 in que1:
					sp2 = roundOff(startPrice(arr2.javaname,2))

					arr2.startprice2 = sp2
					arr2.contractprice2 = sp2
					arr2.save()

					print(arr2.stock+' start price 2 & contract price 2 set to '+str(sp2))
			else:
				print('Today is not desired FRIDAY!')
	except:
		return


def fridayFutures():
	import time
	try:
		print('entered fridayfutures')
		today = datetime.date.today().weekday()

		if(today != 5):
			print('Today is not friday')
			return

		today = time.strftime('%m %d %Y',time.localtime())
		status = 1

		que1 = FuturesOpenedOrClosed.objects.filter(expiry=today,status=status)
		print('getting to it')

		for arr in que1:
			stock = arr.stock
			profitcp = arr.profitcp
			lors = arr.lors
			quantity = arr.quantity
			margin = arr.margin
			opencp = arr.opencp
			user = arr.userid
			order = arr.orderid
			print('values OK')
			nameChunk = stock.split('.')
			name = nameChunk[0]
			try:
				que2 = StocksRate.objects.get(stock=name)
			except:
				continue

			javacp = que2.price
			if(lors=='Long'):
				flag = 1
			else:
				flag = -1

			profit = (javacp - profitcp)*flag*quantity*margin
			status = 0
			que3 = WudRankings.objects.get(userid=user)
			balance = que3.reserve+profit
			equity = que3.equity+profit
			margina = que3.margin-(quantity*margin*opencp)

			que3.reserve = balance
			que3.equity = equity
			que3.margin = margina
			que3.save()

			arr.status =  status
			arr.profit = profit
			arr.save()

			print('Account of '+user+' updated by Rs '+str(profit))
	except:
		return


def startUpdate():
	try:
		print('entered startUpdate')
		que = FuturesBase.objects.all()

		today = datetime.date.today().weekday()

		for arr in que:
			sp2 = arr.startprice2
			sp1 = sp2
			#sp2 = sp3              #Comment this line during last week
			sp3 = roundOff(startPrice(arr.javaname,3)) #Comment this line during 2nd last week

			que2 = FuturesBase.objects.get(stock=arr.stock)
			que2.startprice1=sp1
			que2.startprice2 = sp2
			que2.startprice3 = sp3
			que2.contractprice1 = sp1
			que2.contractprice2 = sp2
			que2.contractprice3 = sp3
			que2.save()

			print(arr.stock+' start price 1 & contract price 1 set to '+str(sp1))
			print(arr.stock+' start price 2 & contract price 2 set to '+str(sp2))
			print(arr.stock+' start price 3 & contract price 3 set to '+str(sp3))
	except:
		return

