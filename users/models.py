from django.db import models
from django.db.models.fields.related import OneToOneField
from django.conf import settings

USER_TYPES=(("3","Admin"),
            ("1","User"),
            ("2","Member")
            );
            
#Profile Manager for Profile Table Queries (Add functionality if needed)          
class ProfileManager(models.Manager):
    
    use_for_related_fields=True;
    
    def getByYear(self,year):
        return Profile.objects.filter(Year=year);
    
# Profile Model that holds Profile Information
class Profile(models.Model):
    user=OneToOneField(settings.AUTH_USER_MODEL)
    Gender=models.CharField(max_length=1)
    DateOfBirth=models.DateField()
    Institution=models.CharField(max_length=200)
    Country=models.CharField(max_length=100)
    #City=models.CharField(max_length=100,default="szdszd",editable=False)
    State=models.CharField(max_length=100)
    Department=models.CharField(max_length=100)
    Year=models.IntegerField()
    Contact=models.CharField(max_length=20)
    Address=models.TextField()
    #ProfilePic=models.TextField(default="sasd",editable=False)
    RegDate=models.DateTimeField()
    UserType=models.IntegerField(choices=USER_TYPES)
    Social=models.CharField(default="",max_length=10)
    

    #Overriding Manager
    objects=ProfileManager();
    
    def __unicode__(self):
        return self.user.username;
    


class State(models.Model):
    state=models.CharField(max_length=100)
    def user_count(self):
        return Profile.objects.filter(State = self.state).count()
    
    def __unicode__(self):
        return self.state
    
class Department(models.Model):
    department=models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.department

class Institute(models.Model):
    state=models.ForeignKey(State)
    institute=models.CharField(max_length=100)
    def user_count(self):
        return Profile.objects.filter(Institution = self.institute).count()
    def __unicode__(self):
        return self.institute
    
class OldUsers(models.Model):
    email = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    Institution = models.CharField(max_length=100)
