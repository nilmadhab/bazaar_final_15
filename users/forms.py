'''
Created on Jun 10, 2013

@author: Srinivasan
'''
from django import forms
from django.contrib.auth.models import User
from django.db import transaction
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.auth import authenticate,login 
from django.utils import timezone
from users.models import Profile,Institute,Department
from django.utils.translation import pgettext
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _
from . import app_settings
from urllib2 import urlopen
from json import loads
from models import State
from utils import profile_exists
from app_settings import AuthenticationMethod
from index.models import ForexRankings
from data.models import WudRankings

from django.http.response import HttpResponseRedirect, HttpResponse

BIRTH_YEAR_CHOICES=('1980', '1981', '1982','1983', '1984', '1985','1986', '1987', '1988','1989', '1990', '1991','1992', '1993', '1994','1995', '1996', '1997','1998', '1999', '2000','2001', '2002', '2003','2004', '2005', '2006','2007', '2008', '2009');

GENDER_CHOICES=(('M','Male'),
                ('F','Female'));
                
YEAR_CHOICES=(('1','1st'),('2','2nd'),('3','3rd'),('4','4th'),('5','5th'),('6','Postgraduate Ist'),('7','Postgraduate IInd'),('8','Postgraduate IIIrd'),('9','Other'));

STATES={'':'Choose State'}
INSTITUTES={'':'Choose State'}
DEPARTMENT={}

class PasswordField(forms.CharField):

    def __init__(self, *args, **kwargs):
        render_value = kwargs.pop('render_value', 
                                  app_settings.PASSWORD_INPUT_RENDER_VALUE)
        kwargs['widget'] = forms.PasswordInput(render_value=render_value, 
                                               attrs={'placeholder': _('Password')})
        super(PasswordField, self).__init__(*args, **kwargs)

class SetPasswordField(PasswordField):

    def clean(self, value):
        value = super(SetPasswordField, self).clean(value)
        min_length = app_settings.PASSWORD_MIN_LENGTH
        if len(value) < min_length:
            raise forms.ValidationError(_("Password must be a minimum of {0} "
                                          "characters.").format(min_length))
        return value


class ProfileForm(forms.Form):
    username=forms.CharField(max_length=30,label='Username')
    email=forms.EmailField(max_length=75,label='Email')
    password1 = SetPasswordField(label=_("Password"),required=False)
    password2 = PasswordField(label=_("Password (again)"),required=False)
    firstname=forms.CharField(max_length=30,label='Firstname')
    lastname=forms.CharField(max_length=30,label='Lastname')
    gender=forms.ChoiceField(choices=GENDER_CHOICES,label='Gender')
    dateofbirth=forms.DateField(label='Date Of Birth',widget = SelectDateWidget(years=BIRTH_YEAR_CHOICES,))
    institution=forms.ChoiceField(choices=INSTITUTES,label='Institution')
    country=forms.CharField(max_length=100,label='Country')
    state=forms.ChoiceField(choices=STATES,label='State')
    department=forms.ChoiceField(choices=DEPARTMENT,label='Department')
    year=forms.ChoiceField(choices=YEAR_CHOICES,label='Year')
    contact=forms.CharField(max_length=20,label='Contact')
    address=forms.CharField(widget=forms.Textarea)
    social=forms.CharField(required=False)
    #profilepic=forms.CharField()
    
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        st=State.objects.all()
        for stat in st:
            STATES[stat.state]=stat.state
        self.fields['state'].choices=STATES.items()
        insti=Institute.objects.all()
        for inst in insti:
           INSTITUTES[inst.institute]=inst.institute
            
        dept=Department.objects.all()
        for dep in dept:
            DEPARTMENT[dep.department]=dep.department
        self.fields['institution'].choices=INSTITUTES.items()
        self.fields['department'].choices=DEPARTMENT.items()
        self.fields['email'].required = True
        self.fields['email'].label = 'Email Address'
        
    def clean(self):
        if self._errors:
            return
        ur=User.objects.filter(username=self.cleaned_data['username'])
        if ur.count()>=1:
            raise forms.ValidationError(_("Username Exists"))
        ur=User.objects.filter(email=self.cleaned_data['email'])
        if ur.count()>=1:
            raise forms.ValidationError(_("User with Email Exists"))
        if self.cleaned_data['password1']!=self.cleaned_data['password2']:
            raise forms.ValidationError(_("Confirm password not Matching"))
        return self.cleaned_data
        
    def create_user(self, commit=True):
        if app_settings.USERNAME_REQUIRED:
            username = self.cleaned_data["username"]
        else:
            username = None
        data = self.initial
        user = User(username=username,email=self.cleaned_data["email"].strip().lower())
        user.set_unusable_password()
        if commit:
            user.save()
        password = self.cleaned_data.get("password1")
        if password:
            user.set_password(password)
        if commit:
            user.save()
        return user
    
    
    @transaction.commit_on_success
    def save(self):
        try:
            if self.is_valid():
                user = User()
                user.username=self.cleaned_data['username']
                user.email=self.cleaned_data['email']
                user.set_password(self.cleaned_data['password1'])
                user.first_name=self.cleaned_data['firstname']
                user.last_name=self.cleaned_data['lastname']
                user.save();
                new_user=User.objects.get(pk=user.id)
                profile=Profile();
                profile.user=new_user;
                profile.Gender=self.cleaned_data['gender']
                profile.DateOfBirth=self.cleaned_data['dateofbirth']
                profile.Institution=self.cleaned_data['institution']
                profile.Country=self.cleaned_data['country']
                #profile.City=self.cleaned_data['city']
                profile.State=self.cleaned_data['state']
                profile.Department=self.cleaned_data['department']
                profile.Year=self.cleaned_data['year']
                profile.Contact=self.cleaned_data['contact']
                profile.Address=self.cleaned_data['address']
                #profile.ProfilePic=self.cleaned_data['profilepic']
                profile.RegDate=timezone.now()
                profile.UserType=1
                profile.save()
        except:
            if user is not None:
                user.delete()
            

class SocialForm(forms.Form):
    username=forms.CharField(max_length=30,label='Username')
    email=forms.EmailField(max_length=75,label='Email')
    firstname=forms.CharField(max_length=30,label='Firstname')
    lastname=forms.CharField(max_length=30,label='Lastname')
    gender=forms.ChoiceField(choices=GENDER_CHOICES,label='Gender')
    dateofbirth=forms.DateField(label='Date Of Birth',widget = SelectDateWidget(years=BIRTH_YEAR_CHOICES,))
    institution=forms.ChoiceField(choices=INSTITUTES,label='Institution')
    country=forms.CharField(max_length=100,label='Country')
    state=forms.ChoiceField(choices=STATES,label='State')
    department=forms.ChoiceField(choices=DEPARTMENT,label='Department')
    year=forms.ChoiceField(choices=YEAR_CHOICES,label='Year')
    contact=forms.CharField(label='Contact')
    address=forms.CharField(widget=forms.Textarea)
    social=forms.CharField(required=False)
    #profilepic=forms.CharField()
    
    def __init__(self, *args, **kwargs):
        super(SocialForm, self).__init__(*args, **kwargs)
        st=State.objects.all()
        for stat in st:
            STATES[stat.state]=stat.state
        self.fields['state'].choices=STATES.items()
        insti=Institute.objects.all()
        for inst in insti:
            INSTITUTES[inst.institute]=inst.institute
            
        dept=Department.objects.all()
        for dep in dept:
            DEPARTMENT[dep.department]=dep.department
        self.fields['institution'].choices=INSTITUTES.items()
        self.fields['department'].choices=DEPARTMENT.items()
        self.fields['email'].required = True
        self.fields['email'].label = 'Email Address'
        
    def clean(self):
        if self._errors:
            return
        ur=User.objects.filter(username=self.cleaned_data['username'])
        if ur.count()>=1:
            raise forms.ValidationError(_("Username Exists"))
        ur=User.objects.filter(email=self.cleaned_data['email'])
        if ur.count()>=1:
            raise forms.ValidationError(_("User with Email Exists"))
        return self.cleaned_data
        
    def create_user(self, commit=True):
        if app_settings.USERNAME_REQUIRED:
            username = self.cleaned_data["username"]
        else:
            username = None
        data = self.initial
        user = User(username=username,email=self.cleaned_data["email"].strip().lower())
        user.set_unusable_password()
        if commit:
            user.save()
        password = timezone.now()
        if password:
            user.set_password(password)
        if commit:
            user.save()
        return user
    
    
    @transaction.commit_on_success
    def save(self):
        try:
            if self.is_valid():
                user = User()
                user.username=self.cleaned_data['social']+'_'+self.cleaned_data['username']
                user.email=self.cleaned_data['email']
                user.set_password(timezone.now())
                user.first_name=self.cleaned_data['firstname']
                user.last_name=self.cleaned_data['lastname']
                user.save();
                new_user=User.objects.get(pk=user.id)
                profile=Profile();
                profile.user=new_user;
                profile.Gender=self.cleaned_data['gender']
                profile.DateOfBirth=self.cleaned_data['dateofbirth']
                profile.Institution=self.cleaned_data['institution']
                profile.Country=self.cleaned_data['country']
                #profile.City=self.cleaned_data['city']
                profile.State=self.cleaned_data['state']
                profile.Department=self.cleaned_data['department']
                profile.Year=self.cleaned_data['year']
                profile.Contact=self.cleaned_data['contact']
                profile.Address=self.cleaned_data['address']
                #profile.ProfilePic=self.cleaned_data['profilepic']
                profile.RegDate=timezone.now()
                profile.UserType=1
                profile.save()
        except:
            if user is not None:
                user.delete()


            
class LoginForm(forms.Form):
    
    password = PasswordField(label = _("Password"))
    remember = forms.BooleanField(label = _("Remember Me"),required = False)
    user = None
    
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
            login_widget = forms.TextInput(attrs={'placeholder': 
                                                  _('E-mail address') })
            login_field = forms.EmailField(label=_("E-mail"),
                                           widget=login_widget)
        elif app_settings.AUTHENTICATION_METHOD \
                == AuthenticationMethod.USERNAME:
            login_widget = forms.TextInput(attrs={'placeholder': 
                                                  _('Username') })
            login_field = forms.CharField(label=_("Username"),
                                          widget=login_widget,
                                          max_length=30)
        else:
            assert app_settings.AUTHENTICATION_METHOD \
                == AuthenticationMethod.USERNAME_EMAIL
            login_widget = forms.TextInput(attrs={'placeholder': 
                                                  _('Username or e-mail') })
            login_field = forms.CharField(label=pgettext("field label", "Login"),
                                          widget=login_widget)
        self.fields["login"] = login_field
        self.fields.keyOrder = ["login", "password", "remember"]
    
    def user_credentials(self):
        """
        Provides the credentials required to authenticate the user for
        login.
        """
        credentials = {}
        login = self.cleaned_data["login"]
        if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
            credentials["email"] = login
        elif (app_settings.AUTHENTICATION_METHOD 
              == AuthenticationMethod.USERNAME):
            credentials["username"] = login
        else:
            if "@" in login and "." in login:
                credentials["email"] = login
            credentials["username"] = login
        credentials["password"] = self.cleaned_data["password"]
        return credentials
    
    def clean(self):
        if self._errors:
            return
        user = authenticate(**self.user_credentials())
        if user:
            if user.is_active:
                self.user = user
            else:
                raise forms.ValidationError(_("This account is currently"
                                              " inactive."))
        else:
            if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
                error = _("The e-mail address and/or password you specified"
                          " are not correct.")
            elif app_settings.AUTHENTICATION_METHOD \
                  == AuthenticationMethod.USERNAME:
                error = _("The username and/or password you specified are"
                          " not correct.")
            else:
                error = _("The login and/or password you specified are not"
                          " correct.")
            raise forms.ValidationError(error)
        return self.cleaned_data
    
    def get_success_url(self):
        return '/forex'
    
    def get_fail_url(self):

        #return '/accounts/login'
        return '/accounts/login'
    
    def login(self, request, redirect_url=None):
        user=self.user
        request.session['user']=user.username
        request.session['user_id']=user.id
        if user is not None:
            if user.is_active:
                try:
                    que = WudRankings.objects.filter(userid=user.username)
                    if(que.count() == 0):
                        que = ForexRankings.objects.get(userid=user.username)
                    login(request, user)
                    ret=self.get_success_url()
                except:
                    ret=self.get_fail_url()
            else:
                ret=self.get_fail_url()
        else:
            ret=self.get_fail_url()
        if self.cleaned_data["remember"]:
            request.session.set_expiry(60 * 60 * 24 * 7 * 3)
        else:
            request.session.set_expiry(0)
        return HttpResponseRedirect(ret)



class SocialLoginForm(forms.Form):
    username= forms.CharField()
    email=forms.EmailField()
    access_token=forms.CharField()
    social=forms.CharField()
    user = None
    
    def __init__(self, *args, **kwargs):
        super(SocialLoginForm, self).__init__(*args, **kwargs)
        
    def user_credentials(self):
        """
        Provides the credentials required to authenticate the user for
        login.
        """
        credentials = {}
        username = self.cleaned_data["username"]
        email=self.cleaned_data['email']
        access_token=self.cleaned_data['access_token']
        social=self.cleaned_data['social']
        return credentials
    
    '''def authenticate_server_FB(self,username,email,access_token):
        url = "https://graph.facebook.com/me?access_token="+access_token
        dict=loads(urlopen(url).read())
        if(dict['username']==username and dict['email']==email):
            return True
        return False
    
    def authenticate_server_GP(self,username,email,access_token):
        url = "https://www.googleapis.com/oauth2/v1/userinfo?access_token="+access_token
        dict=loads(urlopen(url).read())
        if(dict['name']==username and dict['email']==email):
            return True
        return False'''
    
    def clean(self):
        if self._errors:
            return
        '''if(self.cleaned_data['social']=='FB'):
            if(self.authenticate_server_FB(self.cleaned_data['username'],self.cleaned_data['email'],self.cleaned_data['access_token'])==False):
                raise forms.ValidationError(_("Invalid Login"))
        elif (self.cleaned_data['social']=='GP'):
            if(self.authenticate_server_GP(self.cleaned_data['username'],self.cleaned_data['email'],self.cleaned_data['access_token'])==False):
                raise forms.ValidationError(_("Invalid Login"))'''
        if profile_exists(self.cleaned_data['social']+'_'+self.cleaned_data['username'],self.cleaned_data['email']):
            user=authenticate(username=self.cleaned_data['social']+'_'+self.cleaned_data['username'],password=None,email=self.cleaned_data['email'],access_token=self.cleaned_data['access_token'])
            print(user)
            if user is not None:
                self.user = user
            else:
                raise forms.ValidationError(_("This account is currently"
                                              " inactive."))
        else:
            error = _("The login and/or password you specified are not"
                          " correct.")
            raise forms.ValidationError(error)
        return self.cleaned_data
    
    def get_success_url(self):
        return '/accounts/check_logged'
    
    def get_fail_url(self):
        return '/accounts/login'
    
    def login(self, request, redirect_url=None):
        print('Hello')
        user=self.user
        request.session['user']=user.username
        request.session['user_id']=user.id
        if user is not None:
            try:
                que = WudRankings.objects.filter(userid=user.username)
                if(que.count() == 0):
                    que = ForexRankings.objects.get(userid=user.username)
                login(request, user)
                ret=self.get_success_url()
            except:
                ret=self.get_fail_url()
        else:
            ret=self.get_fail_url()
            request.session.set_expiry(60 * 60 * 24 * 7 * 3)
        return HttpResponseRedirect(ret)
        
