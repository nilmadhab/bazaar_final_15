'''
Created on Jun 8, 2013

@author: Srinivasan
'''

class UserRouter(object): 
    def db_for_read(self, model, **hints):
        "Point all operations on algotrade models to 'ktj14_beta' database"
        if model._meta.app_label == 'users' or model._meta.app_label == 'auth':
            return 'beta'
        return None

    def db_for_write(self, model, **hints):
        "Point all operations on algotrade models to 'algotrade'"
        return None
    
    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in algotrade app"
        if obj1._meta.app_label == 'users' and obj2._meta.app_label == 'users':
            return True
        elif 'users' not in [obj1._meta.app_label, obj2._meta.app_label]: 
            return True
        return None
    
    def allow_syncdb(self, db, model):
        if db == 'beta' and model._meta.app_label == "beta":
            return True
        elif db == 'beta' or model._meta.app_label == "beta":
            return False
        else:
            return None
