from django.conf.urls import patterns, url
from users import views
from django.views.decorators.csrf import csrf_exempt


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ktj14.views.home', name='home'),
    url(r'^nil/', views.nil),
    url(r'^', csrf_exempt(views.login) , name="login"),
    url(r'^login$', csrf_exempt(views.login) , name="account_login"),
    url(r'^logout$', csrf_exempt(views.logout_ktj) , name="account_logout"),
    url(r'^success$', csrf_exempt(views.success) , name="account_success"),
    url(r'^social_login$', csrf_exempt(views.social_login) , name="social_login"),
    url(r'^check_logged$', csrf_exempt(views.check_logged) , name="check_logged"),
    url(r'^profile_exists$', csrf_exempt(views.profile_exists) , name="profile_exists"),
    url(r'^finalists$', views.wsfxFinalists, name='wsfx_finalists'),
   

)
